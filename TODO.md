* Implement Matrix functions again in C for compilation with emscripten
* Use emscripten to template JS instead of raw calls
* Surface of Revolution
* Parametric Surfaces
* Texture Mapping
* Raytracing
