#include "Mat4.hpp"

Mat4::Mat4() {
    array = new double[16];
    // Identity
    array[0] = 1;
    array[1] = 0;
    array[2] = 0;
    array[3] = 0;
    array[4] = 0;
    array[5] = 1;
    array[6] = 0;
    array[7] = 0;
    array[8] = 0;
    array[9] = 0;
    array[10] = 1;
    array[11] = 0;
    array[12] = 0;
    array[13] = 0;
    array[14] = 0;
    array[15] = 1;
}

Mat4::Mat4(const Mat4 &obj) {
    array = new double[16];
    array[0] = obj.array[0];
    array[1] = obj.array[1];
    array[2] = obj.array[2];
    array[3] = obj.array[3];
    array[4] = obj.array[4];
    array[5] = obj.array[5];
    array[6] = obj.array[6];
    array[7] = obj.array[7];
    array[8] = obj.array[8];
    array[9] = obj.array[9];
    array[10] = obj.array[10];
    array[11] = obj.array[11];
    array[12] = obj.array[12];
    array[13] = obj.array[13];
    array[14] = obj.array[14];
    array[15] = obj.array[15];
}

Mat4::~Mat4() {
    delete array;
}

void Mat4::transpose() {
    double a01 = array[1], a02 = array[2], a03 = array[3],
    a12 = array[6], a13 = array[7],
    a23 = array[11];

    array[1] = array[4];
    array[2] = array[8];
    array[3] = array[12];
    array[4] = a01;
    array[6] = array[9];
    array[7] = array[13];
    array[8] = a02;
    array[9] = a12;
    array[11] = array[14];
    array[12] = a03;
    array[13] = a13;
    array[14] = a23;
}

void Mat4::translate(double x, double y, double z) {
    array[12] = array[0] * x + array[4] * y + array[8] * z + array[12];
    array[13] = array[1] * x + array[5] * y + array[9] * z + array[13];
    array[14] = array[2] * x + array[6] * y + array[10] * z + array[14];
    array[15] = array[3] * x + array[7] * y + array[11] * z + array[15];
}

void Mat4::rotate(double angle, double x, double y, double z) {
    double
        len = sqrt(x * x + y * y + z * z),
        s, c, t,
        a00, a01, a02, a03,
        a10, a11, a12, a13,
        a20, a21, a22, a23,
        b00, b01, b02,
        b10, b11, b12,
        b20, b21, b22;

    if (!len) { return; }
    if (len != 1) {
        len = 1 / len;
        x *= len;
        y *= len;
        z *= len;
    }

    s = sin(angle);
    c = cos(angle);
    t = 1 - c;

    a00 = array[0]; a01 = array[1]; a02 = array[2]; a03 = array[3];
    a10 = array[4]; a11 = array[5]; a12 = array[6]; a13 = array[7];
    a20 = array[8]; a21 = array[9]; a22 = array[10]; a23 = array[11];

    // Construct the elements of the rotation matrix
    b00 = x * x * t + c; b01 = y * x * t + z * s; b02 = z * x * t - y * s;
    b10 = x * y * t - z * s; b11 = y * y * t + c; b12 = z * y * t + x * s;
    b20 = x * z * t + y * s; b21 = y * z * t - x * s; b22 = z * z * t + c;

    // Perform rotation-specific matrix multiplication
    array[0] = a00 * b00 + a10 * b01 + a20 * b02;
    array[1] = a01 * b00 + a11 * b01 + a21 * b02;
    array[2] = a02 * b00 + a12 * b01 + a22 * b02;
    array[3] = a03 * b00 + a13 * b01 + a23 * b02;

    array[4] = a00 * b10 + a10 * b11 + a20 * b12;
    array[5] = a01 * b10 + a11 * b11 + a21 * b12;
    array[6] = a02 * b10 + a12 * b11 + a22 * b12;
    array[7] = a03 * b10 + a13 * b11 + a23 * b12;

    array[8] = a00 * b20 + a10 * b21 + a20 * b22;
    array[9] = a01 * b20 + a11 * b21 + a21 * b22;
    array[10] = a02 * b20 + a12 * b21 + a22 * b22;
    array[11] = a03 * b20 + a13 * b21 + a23 * b22;
}

void Mat4::scale(double x, double y, double z) {
    array[0] *= x;
    array[1] *= x;
    array[2] *= x;
    array[3] *= x;
    array[4] *= y;
    array[5] *= y;
    array[6] *= y;
    array[7] *= y;
    array[8] *= z;
    array[9] *= z;
    array[10] *= z;
    array[11] *= z;
}
