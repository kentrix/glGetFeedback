#include <vector>
#include <math.h>

class Mat4 {
    public:
        Mat4();
        Mat4(const Mat4 &obj);
        ~Mat4();
        void translate(double, double, double);
        void rotate(double, double, double, double);
        void scale(double, double, double);
        void transpose();
        void deconstruct(double*, double*, double*);
        double determinant(void);
    private:
        double* array;
};
