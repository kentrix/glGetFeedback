// Utilities.h: header with prototypes of utility functions.
// Copyright: (c) 1997 Burkhard Wuensche
//////////////////////////////////////////////////////////////////////

#if !defined(LECTURE_372FC_UTILITIES)
#define LECTURE_372FC_UTILITIES

#include <iostream>
#include <fstream>
using namespace std;

/////////////////////////////////
//    basic math utilities     //
/////////////////////////////////

float myfmax(float a, float b);
float myfmin(float a, float b);
int myimax(int a, int b);
int myimin(int a, int b);
float myfsqr(float a);


///////////////////////////////////
//    input/output utilities     //
///////////////////////////////////

// skips line when reading an input file
void skipLine(ifstream& s);

////////////////////////////////////
//    2D array of 3D vertices     //
//	  ( stored as float[3] )      //
////////////////////////////////////

class CVertexArray2D
{
public:
	CVertexArray2D();
	CVertexArray2D(int m, int n); // (rows, columns)
	virtual ~CVertexArray2D();
	void setSize(int m, int n); // (rows, columns)
	int getN() const { return sizeN;}
	int getM() const { return sizeM;}
	bool isEmpty() { return (data==NULL);}
	float** operator[](int row);			// returns a column of vertices 
	float* operator()(int row, int column);	// returns a vertex (type float[3])
	friend ostream& operator<<(ostream&, CVertexArray2D&);
protected:
	float*** data;	// the actual array of data
	int sizeM;		// # of rows 
	int sizeN;		// # of columns 
};


///////////////////////////////////
//      random numbers           //
///////////////////////////////////

void sgenrand(unsigned long seed);
unsigned long genrand();
double genrand2();

// integer random number in [min,max]
inline int randomInt(int min, int max)
{
	return min+genrand()%(max-min+1);
}

// double random number in [min,max]
inline double randomDouble(float min, double max)
{
	return min+(genrand2())*(max-min);
}

#endif // !defined(LECTURE_372FC_UTILITIES)
