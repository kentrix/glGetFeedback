#include "Geometry.h"
// returns relected light intensity
double phongIlluminationAchromaticSpecular(
        double ambientIntensity,
        double diffuseIntensity,
        double specularIntensity,
        double ambientReflecCoef,
        double diffuseReflecCoef,
        double specularReflecCoef,
        double shininess,
        double kc,
        double kl,
        double kq,
        CVec3df pointOnSurface,
        CVec3df surfaceNormal,
        CVec3df lightPosition,
        CVec3df viewPoint) {

	CVec3df s = lightPosition - pointOnSurface;
	double d = length(s);	// d is distance from point on surface to light source
	CVec3df v = viewPoint - pointOnSurface;
	CVec3df m = surfaceNormal;
	
	// in order to make subsequent calculations more efficient we normalise s, v, and m
	s.normaliseDestructive();
	v.normaliseDestructive();
	m.normaliseDestructive();
	
	// calculat half-way vector h - note that s and v are already normalised
	CVec3df h = s + v; 
	h.normaliseDestructive();

	return (2*specularIntensity*specularReflecCoef*pow((double) h.dot(m), (double) shininess))/(kc+kl*d+kq*d*d);
}

double phongIlluminationAchromaticDiffuse(
        double ambientIntensity,
        double diffuseIntensity,
        double specularIntensity,
        double ambientReflecCoef,
        double diffuseReflecCoef,
        double specularReflecCoef,
        double shininess,
        double kc,
        double kl,
        double kq,
        CVec3df pointOnSurface,
        CVec3df surfaceNormal,
        CVec3df lightPosition,
        CVec3df viewPoint) {

	CVec3df s = lightPosition - pointOnSurface; // s is the light vector
	double d = length(s);
	CVec3df m=surfaceNormal;
	s.normaliseDestructive();
	m.normaliseDestructive();
	
	return (diffuseIntensity*diffuseReflecCoef*s.dot(m))/(kc+kl*d+kq*d*d);
}

double phongIlluminationAchromaticAmbient(
        double ambientIntensity,
        double diffuseIntensity,
        double specularIntensity,
        double ambientReflecCoef,
        double diffuseReflecCoef,
        double specularReflecCoef,
        double shininess,
        double kc,
        double kl,
        double kq,
        CVec3df pointOnSurface,
        CVec3df surfaceNormal,
        CVec3df lightPosition,
        CVec3df viewPoint) {
    return ambientIntensity * ambientReflecCoef;
}
