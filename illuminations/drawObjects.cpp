// A basic OpenGL program illustrating Phong Illuminatioon
// Burkhard Wuensche, 19 April 2017

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <iostream>
using namespace std;
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include "Geometry.h"

#ifdef CODERUNNER
#include "coderunneropengl.hpp"
#endif /* CODERUNNER */

const int windowWidth=500;
const int windowHeight=500;

const double tolerance = 0.01f;

const double ambientIntensity=0.2f;
const double diffuseIntensity=0.9f;
const double specularIntensity=0.9f;
const double ambientReflecCoef=0.8f;
const double diffuseReflecCoef=0.9f;
const double specularReflecCoef=0.99f;
const double shininess = 20.0f;
const double kc=1.0f;
const double kl=0.2f;
const double kq=0.1f;
const double _ = 1.;
const CVec3df __ = CVec3df(1., 1., 1.);
CVec3df lightPosition(1.0f, 1.0f, 3.0f);
CVec3df viewPoint(0.0f, 0.0f, 5.0f);
CVec3df pointOnSurface;
CVec3df surfaceNormal;

string correctnessString = "";
#ifdef GEN_FEEDBACK
string feedbackString = "";
#endif


inline void drawString(const char* s, void* font=GLUT_BITMAP_TIMES_ROMAN_24){
	while(*s!='\0') glutBitmapCharacter(font, *(s++));
}

inline int withinTolerance(double answer, double model, double tolerance) {
    if(isnan(answer) && isnan(model)) return 1;
    return model + tolerance > answer && model - tolerance < answer;
}

CVec3df p(float s, float t)
{
	return CVec3df(cos(Pi*t)*cos(2*Pi*s), cos(Pi*t)*sin(2*Pi*s), sin(Pi*t));
}
double phongIlluminationAchromaticSpecular__MODEL__(
        __attribute__((unused)) double ambientIntensity,
        __attribute__((unused))double diffuseIntensity,
        double specularIntensity,
        __attribute__((unused)) double ambientReflecCoef,
        __attribute__((unused))double diffuseReflecCoef,
        double specularReflecCoef,
        double shininess,
        double kc,
        double kl,
        double kq,
        CVec3df pointOnSurface,
        CVec3df surfaceNormal,
        CVec3df lightPosition,
        __attribute__((unused)) CVec3df viewPoint) {

	CVec3df s = lightPosition - pointOnSurface;
	double d = length(s);	// d is distance from point on surface to light source
	CVec3df v = viewPoint - pointOnSurface;
	CVec3df m = surfaceNormal;
	
	// in order to make subsequent calculations more efficient we normalise s, v, and m
	s.normaliseDestructive();
	v.normaliseDestructive();
	m.normaliseDestructive();
	
	// calculat half-way vector h - note that s and v are already normalised
	CVec3df h = s + v; 
	h.normaliseDestructive();

	return (specularIntensity*specularReflecCoef*pow((double) h.dot(m), (double) shininess))/(kc+kl*d+kq*d*d);
}

void verifySpecular() {
    // Trivial distance
    phongIlluminationAchromaticSpecular__MODEL__(_, _, specularIntensity, _, _, specularReflecCoef, shininess, 1., 1., 1., pointOnSurface, surfaceNormal, lightPosition, viewPoint);
    phongIlluminationAchromaticSpecular__MODEL__(_, _, _, _, _, _, _, kc, kl, kq, pointOnSurface, surfaceNormal, lightPosition, viewPoint);

}

double phongIlluminationAchromaticDiffuse__MODEL__(
        __attribute__((unused)) double ambientIntensity,
        double diffuseIntensity,
        __attribute__((unused)) double specularIntensity,
        __attribute__((unused)) double ambientReflecCoef,
        double diffuseReflecCoef,
        __attribute__((unused)) double specularReflecCoef,
        __attribute__((unused)) double shininess,
        double kc,
        double kl,
        double kq,
        CVec3df pointOnSurface,
        CVec3df surfaceNormal,
        CVec3df lightPosition,
        __attribute__((unused)) CVec3df viewPoint) {

	CVec3df s = lightPosition - pointOnSurface; // s is the light vector
	double d = length(s);
	CVec3df m=surfaceNormal;
	s.normaliseDestructive();
	m.normaliseDestructive();
	
	return (diffuseIntensity*diffuseReflecCoef*s.dot(m))/(kc+kl*d+kq*d*d);
}

void verifyDiffuseDistance() {
    // Trivial distance factor
    phongIlluminationAchromaticDiffuse__MODEL__(_, diffuseIntensity, _, _, diffuseReflecCoef, _, _, 1.0, 1.0, 1.0, pointOnSurface, surfaceNormal, lightPosition, viewPoint);
    // Trivail coef
    phongIlluminationAchromaticDiffuse__MODEL__(_, _, _, _, _, _, _, kc, kl, kq, pointOnSurface, surfaceNormal, lightPosition, viewPoint);
}

double phongIlluminationAchromaticAmbient__MODEL__(
        double ambientIntensity,
        __attribute__((unused)) double diffuseIntensity,
        __attribute__((unused)) double specularIntensity,
        double ambientReflecCoef,
        __attribute__((unused)) double diffuseReflecCoef,
        __attribute__((unused)) double specularReflecCoef,
        __attribute__((unused)) double shininess,
        __attribute__((unused)) double kc,
        __attribute__((unused)) double kl,
        __attribute__((unused)) double kq,
        __attribute__((unused)) CVec3df pointOnSurface,
        __attribute__((unused)) CVec3df surfaceNormal,
        __attribute__((unused)) CVec3df lightPosition,
        __attribute__((unused)) CVec3df viewPoint) {
    return ambientIntensity * ambientReflecCoef;
}

#ifdef CODERUNNER
#include "answer.hpp"
#else
double phongIlluminationAchromaticSpecular(
        double ambientIntensity,
        __attribute__((unused))double diffuseIntensity,
        double specularIntensity,
        double ambientReflecCoef,
        __attribute__((unused))double diffuseReflecCoef,
        double specularReflecCoef,
        double shininess,
        double kc,
        double kl,
        double kq,
        CVec3df pointOnSurface,
        CVec3df surfaceNormal,
        CVec3df lightPosition,
        CVec3df viewPoint) {

    return phongIlluminationAchromaticSpecular__MODEL__(ambientIntensity,
            diffuseIntensity,
            specularIntensity,
            ambientReflecCoef,
            diffuseReflecCoef,
            specularReflecCoef,
            shininess,
            kc, kl, kq,
            pointOnSurface,
            surfaceNormal,
            lightPosition,
            viewPoint);
}

double phongIlluminationAchromaticDiffuse(
        double ambientIntensity,
        double diffuseIntensity,
        double specularIntensity,
        double ambientReflecCoef,
        double diffuseReflecCoef,
        double specularReflecCoef,
        double shininess,
        double kc,
        double kl,
        double kq,
        CVec3df pointOnSurface,
        CVec3df surfaceNormal,
        CVec3df lightPosition,
        CVec3df viewPoint) {
    return phongIlluminationAchromaticDiffuse__MODEL__(ambientIntensity,
            diffuseIntensity,
            specularIntensity,
            ambientReflecCoef,
            diffuseReflecCoef,
            specularReflecCoef,
            shininess,
            kc, kl, kq,
            pointOnSurface,
            surfaceNormal,
            lightPosition,
            viewPoint);
}

double phongIlluminationAchromaticAmbient(
        double ambientIntensity,
        double diffuseIntensity,
        double specularIntensity,
        double ambientReflecCoef,
        double diffuseReflecCoef,
        double specularReflecCoef,
        double shininess,
        double kc,
        double kl,
        double kq,
        CVec3df pointOnSurface,
        CVec3df surfaceNormal,
        CVec3df lightPosition,
        CVec3df viewPoint) {
    return phongIlluminationAchromaticAmbient__MODEL__(
            ambientIntensity,
            diffuseIntensity,
            specularIntensity,
            ambientReflecCoef,
            diffuseReflecCoef,
            specularReflecCoef,
            shininess,
            kc, kl, kq,
            pointOnSurface,
            surfaceNormal,
            lightPosition,
            viewPoint);
}
/*
// returns relected light intensity
double phongIlluminationAchromatic(double ambientIntensity, double diffuseIntensity, double specularIntensity,
								 double ambientReflecCoef, double diffuseReflecCoef, double specularReflecCoef,
								 double shininess,
								 double kc, double kl, double kq,
								 CVec3df pointOnSurface, CVec3df surfaceNormal, CVec3df lightPosition, CVec3df viewPoint)
{
	CVec3df s=lightPosition-pointOnSurface;					// s is the light vector
	double d=length(s);										// d is distance from point on surface to light source
	CVec3df v=viewPoint-pointOnSurface;						// v is the view vector
	CVec3df m=surfaceNormal;									// m is the surface normal
	
	// in order to make subsequent calculations more efficient we normalise s, v, and m
	s.normaliseDestructive();
	v.normaliseDestructive();
	m.normaliseDestructive();
	
	// calculat half-way vector h - note that s and v are already normalised
	CVec3df h=s+v; 
	h.normaliseDestructive();

	// Implement the achromatic version of the Phong illumination equation from the lecture notes 
	return ambientIntensity*ambientReflecCoef+(diffuseIntensity*diffuseReflecCoef*s.dot(m)+specularIntensity*specularReflecCoef*pow((double) h.dot(m), (double) shininess))/(kc+kl*d+kq*d*d);
}
*/
#endif


void drawParametricSurface()
{
	int numSegmentsS=30;
	int numSegmentsT=30;
	double c;	// phong illumination achromatic at vertex

	int i,j;
	double s,t,ds;
	CVec3df p0,p1,dpds,n;

	for(i=0;i<numSegmentsS;i++)
	{
		glBegin(GL_QUAD_STRIP);
		for(j=0;j<=numSegmentsT;j++)
		{	
			s=(float) i/(float) numSegmentsS;
			t=(float) j/(float) numSegmentsT;
			ds=1.0f/(float) numSegmentsS;
			p0=p(s,t);
			pointOnSurface=p0;
			surfaceNormal=p0; // only for unit sphere
                        c = phongIlluminationAchromaticAmbient(ambientIntensity, diffuseIntensity, specularIntensity,
										ambientReflecCoef, diffuseReflecCoef, specularReflecCoef,
										shininess,
										kc, kl, kq,
										pointOnSurface, surfaceNormal, lightPosition, viewPoint) +
                            phongIlluminationAchromaticDiffuse(ambientIntensity, diffuseIntensity, specularIntensity,
										ambientReflecCoef, diffuseReflecCoef, specularReflecCoef,
										shininess,
										kc, kl, kq,
										pointOnSurface, surfaceNormal, lightPosition, viewPoint) +
                            phongIlluminationAchromaticSpecular(ambientIntensity, diffuseIntensity, specularIntensity,
										ambientReflecCoef, diffuseReflecCoef, specularReflecCoef,
										shininess,
										kc, kl, kq,
										pointOnSurface, surfaceNormal, lightPosition, viewPoint);
			glColor3f(c,c,c);			
			glVertex3fv(p0.getArray());

			p1=p(s+ds,t);
			pointOnSurface=p1;
                        c = phongIlluminationAchromaticAmbient(ambientIntensity, diffuseIntensity, specularIntensity,
										ambientReflecCoef, diffuseReflecCoef, specularReflecCoef,
										shininess,
										kc, kl, kq,
										pointOnSurface, surfaceNormal, lightPosition, viewPoint) +
                            phongIlluminationAchromaticDiffuse(ambientIntensity, diffuseIntensity, specularIntensity,
										ambientReflecCoef, diffuseReflecCoef, specularReflecCoef,
										shininess,
										kc, kl, kq,
										pointOnSurface, surfaceNormal, lightPosition, viewPoint) +
                            phongIlluminationAchromaticSpecular(ambientIntensity, diffuseIntensity, specularIntensity,
										ambientReflecCoef, diffuseReflecCoef, specularReflecCoef,
										shininess,
										kc, kl, kq,
										pointOnSurface, surfaceNormal, lightPosition, viewPoint);
			glColor3f(c,c,c);			
			glVertex3fv(p1.getArray());

		}	
		glEnd();
	}
}


void display(void)
{
    glMatrixMode( GL_MODELVIEW );	// Set the view matrix ...
    glLoadIdentity();				// ... to identity.
    gluLookAt(0,0,5, 0,0,0, 0,1,0); // camera is on the z-axis

    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);

    // draw parametric surface
    drawParametricSurface();

#ifdef CODERUNNER
//#include "testcode.hpp"
#define ITERATIONS 1000
    // We'd like some sort of hspec testing, maybe
    srand(time(NULL));
    int correct = 1;
    // Test ambient fuction
    for(int i = 0; i < ITERATIONS; i++) {
        double inA, inB, inC, coefA, coefB, coefC, shi, ka, kb, kc;
        CVec3df a((float) rand() / RAND_MAX, (float) rand() / RAND_MAX, (float) rand() / RAND_MAX);
        CVec3df b((float) rand() / RAND_MAX, (float) rand() / RAND_MAX, (float) rand() / RAND_MAX);
        CVec3df c((float) rand() / RAND_MAX, (float) rand() / RAND_MAX, (float) rand() / RAND_MAX);
        CVec3df d((float) rand() / RAND_MAX, (float) rand() / RAND_MAX, (float) rand() / RAND_MAX);
        inA = (double) rand() / RAND_MAX;
        inB = (double) rand() / RAND_MAX;
        inC = (double) rand() / RAND_MAX;
        coefA = (double) rand() / RAND_MAX;
        coefB = (double) rand() / RAND_MAX;
        coefC = (double) rand() / RAND_MAX;
        shi = (double) rand() / RAND_MAX;
        ka = (double) rand() / RAND_MAX;
        kb = (double) rand() / RAND_MAX;
        kc = (double) rand() / RAND_MAX;
        
        double student = phongIlluminationAchromaticAmbient(inA, inB, inC, coefA, coefB, coefC, shi, ka, kb, kc, a, b, c, d);
        double model = phongIlluminationAchromaticAmbient__MODEL__(inA, inB, inC, coefA, coefB, coefC, shi, ka, kb, kc, a, b, c, d);

        if(!withinTolerance(student, model, tolerance)) {
            feedbackString += "Your ambient calculation is off.\t";
            correct = 0;
            break;
        }

    }
    // Test diffuse
    for(int i = 0; i < ITERATIONS; i++) {
        double inA, inB, inC, coefA, coefB, coefC, shi, ka, kb, kc;
        CVec3df a((float) rand() / RAND_MAX, (float) rand() / RAND_MAX, (float) rand() / RAND_MAX);
        CVec3df b((float) rand() / RAND_MAX, (float) rand() / RAND_MAX, (float) rand() / RAND_MAX);
        CVec3df c((float) rand() / RAND_MAX, (float) rand() / RAND_MAX, (float) rand() / RAND_MAX);
        CVec3df d((float) rand() / RAND_MAX, (float) rand() / RAND_MAX, (float) rand() / RAND_MAX);
        inA = (double) rand() / RAND_MAX;
        inB = (double) rand() / RAND_MAX;
        inC = (double) rand() / RAND_MAX;
        coefA = (double) rand() / RAND_MAX;
        coefB = (double) rand() / RAND_MAX;
        coefC = (double) rand() / RAND_MAX;
        shi = (double) rand() / RAND_MAX;
        ka = (double) rand() / RAND_MAX;
        kb = (double) rand() / RAND_MAX;
        kc = (double) rand() / RAND_MAX;
        
        double student = phongIlluminationAchromaticDiffuse(inA, inB, inC, coefA, coefB, coefC, shi, ka, kb, kc, a, b, c, d);
        double model = phongIlluminationAchromaticDiffuse__MODEL__(inA, inB, inC, coefA, coefB, coefC, shi, ka, kb, kc, a, b, c, d);
        if(!withinTolerance(student, model, tolerance)) {
            feedbackString += "Your diffuse calculation is off.\t";
            correct = 0;
            if(!withinTolerance(phongIlluminationAchromaticDiffuse(inA, inB, inC, coefA, coefB, coefC, shi, 1.0, 1.0, 1.0, a, b, c, d),
                        phongIlluminationAchromaticDiffuse__MODEL__(inA, inB, inC, coefA, coefB, coefC, shi, 1.0, 1.0, 1.0, a, b, c, d),
                        tolerance))
                {
                feedbackString += "Check your ambient coef calculations!\t";
                break;
                }
            if(!withinTolerance(phongIlluminationAchromaticDiffuse(_, _, _, _, _, _, _, ka, kb, kc, a, b, c, d),
                    phongIlluminationAchromaticDiffuse__MODEL__(_, _, _, _, _, _, _, ka, kb, kc, a, b, c, d),
                    tolerance))
                {
                feedbackString += "Check your ambient distance calculations!\t";
                break;
                }
            break;
        }
    }

    // Test specular
    for(int i = 0; i < ITERATIONS; i++) {
        double inA, inB, inC, coefA, coefB, coefC, shi, ka, kb, kc;
        CVec3df a((float) rand() / RAND_MAX, (float) rand() / RAND_MAX, (float) rand() / RAND_MAX);
        CVec3df b((float) rand() / RAND_MAX, (float) rand() / RAND_MAX, (float) rand() / RAND_MAX);
        CVec3df c((float) rand() / RAND_MAX, (float) rand() / RAND_MAX, (float) rand() / RAND_MAX);
        CVec3df d((float) rand() / RAND_MAX, (float) rand() / RAND_MAX, (float) rand() / RAND_MAX);
        inA = (double) rand() / RAND_MAX;
        inB = (double) rand() / RAND_MAX;
        inC = (double) rand() / RAND_MAX;
        coefA = (double) rand() / RAND_MAX;
        coefB = (double) rand() / RAND_MAX;
        coefC = (double) rand() / RAND_MAX;
        shi = (double) rand() / RAND_MAX;
        ka = (double) rand() / RAND_MAX;
        kb = (double) rand() / RAND_MAX;
        kc = (double) rand() / RAND_MAX;
        
        double student = phongIlluminationAchromaticSpecular(inA, inB, inC, coefA, coefB, coefC, shi, ka, kb, kc, a, b, c, d);
        double model = phongIlluminationAchromaticSpecular__MODEL__(inA, inB, inC, coefA, coefB, coefC, shi, ka, kb, kc, a, b, c, d);
        if(!withinTolerance(student, model, tolerance)) {
            feedbackString += "Your specular calculation is off.\t";
            correct = 0;
            if(!withinTolerance(phongIlluminationAchromaticSpecular(inA, inB, inC, coefA, coefB, coefC, shi, 1.0, 1.0, 1.0, a, b, c, d),
                        phongIlluminationAchromaticSpecular__MODEL__(inA, inB, inC, coefA, coefB, coefC, shi, 1.0, 1.0, 1.0, a, b, c, d),
                        tolerance))
                {
                feedbackString += "Check your specular coef calculations!\t";
                break;
                }
            if(!withinTolerance(phongIlluminationAchromaticSpecular(_, _, _, _, _, _, _, ka, kb, kc, a, b, c, d),
                    phongIlluminationAchromaticSpecular__MODEL__(_, _, _, _, _, _, _, ka, kb, kc, a, b, c, d),
                    tolerance))
                {
                feedbackString += "Check your specular distance calculations!\t";
                break;
                }
            break;
        }
    }
    if(correct) {
        feedbackString += "Well done!";
        correctnessString = "1";
    }
    
#endif
    pointOnSurface.setVector(0.3f, 0.4f, sqrt(0.75));
    surfaceNormal.setVector(0.3f, 0.4f, sqrt(0.75));

    double ambient = phongIlluminationAchromaticAmbient(ambientIntensity, diffuseIntensity, specularIntensity,
            ambientReflecCoef, diffuseReflecCoef, specularReflecCoef,
            shininess,
            kc, kl, kq,
            pointOnSurface, surfaceNormal, lightPosition, viewPoint);
    double diffuse = phongIlluminationAchromaticDiffuse(ambientIntensity, diffuseIntensity, specularIntensity,
            ambientReflecCoef, diffuseReflecCoef, specularReflecCoef,
            shininess,
            kc, kl, kq,
            pointOnSurface, surfaceNormal, lightPosition, viewPoint);
    double specular = phongIlluminationAchromaticSpecular(ambientIntensity, diffuseIntensity, specularIntensity,
            ambientReflecCoef, diffuseReflecCoef, specularReflecCoef,
            shininess,
            kc, kl, kq,
            pointOnSurface, surfaceNormal, lightPosition, viewPoint);

    // draw parametric surface
    drawParametricSurface();

    glFlush();

#ifdef GEN_FEEDBACK
    cout << feedbackString << endl;
#else
    cout << endl;
#endif
    cout << correctnessString << endl;
    cout << endl;

}


void init(void) 
{
	// select clearing color (for glClear)
	glClearColor (1,1,0,1);	// RGB-value for black
	// enable depth buffering
	glEnable(GL_DEPTH_TEST);
	// initialize view (simple orthographic projection)
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(40,1,2,8);
}



// create a double buffered colour window
int main(int argc, char** argv)
{
#ifdef CODERUNNER
	CodeRunnerOpenGL codeRunnerOpenGL(windowWidth, windowHeight);
#else	
	glutInit(&argc, argv);		
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(windowWidth, windowHeight); 
	glutInitWindowPosition(100, 100);
	glutCreateWindow("OpenGL Illumination and Shading");
#endif
	init ();								// initialise view
#ifdef CODERUNNER
	display();
#else
	glutDisplayFunc(display);		// draw scene
	glutMainLoop();
#endif

	return 0;
}
