// A basic OpenGL program displaying OpenGL geometric primitives
// Burkhard Wuensche, 27 Feb 2016

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <stdio.h>
#include <string.h>
#include "drawPrimitives.hpp"

#ifdef CODERUNNER
#include "coderunneropengl.hpp"
#include <png++/png.hpp>
#endif /* CODERUNNER */

// Draws the string s at the current raster position using the given font.
// If no font argument is given GLUT_BITMAP_TIMES_ROMAN_24 is used as default
inline void drawString(const char* s, void* font=GLUT_BITMAP_TIMES_ROMAN_24){
	while(*s!='\0') glutBitmapCharacter(font, *(s++));
}

void display(void)
{
	// clear all pixels in frame buffer
	glClear(GL_COLOR_BUFFER_BIT);
	glPointSize(3.0f);
	int i,dx=110;

	glColor3f(0.0, 0.0, 0.0);	// black colour
	glPointSize(4.0);
	for(i=0;i<VERTICES;i++)
	{
		glRasterPos2f(vertices[i][0]-5, vertices[i][1]+5);
		char* s=new char[2]; sprintf(s,"%d",i); drawString(s);
		glBegin(GL_POINTS);
                glVertex2fv(vertices[i]);
		glEnd();
	}

	glPushMatrix();

	// Draw the geometric objects
	glColor3f(1.0, 0.7, 0.8);	// reddish colour

/*
	const int numIndices=6;	
#ifdef CODERUNNER
#include "answer.hpp"
#else
	int index[numIndices]={0,1,3,5,4,2};	// solution for GL_POLYGON
#endif
	glBegin(GL_POLYGON);
	for(int i=0;i<numIndices;i++)
	{
		glVertex2fv(vertices[index[i]]);
		printf("%d ",index[i]);
	}
	glEnd();
*/

	const int numIndices=6;	
#ifdef CODERUNNER
#include "answer.hpp"
#ifdef GEN_FEEDBACK
#include "primitive_helper.h"
	int model[numIndices]={0,1,2,3,4,5};	// solution for GL_TRIANGLE_STRIP
        gen_feedback(model, index, numIndices, numIndices, GL_TRIANGLE_STRIP);
        // We shall seperate answer checking with feedback and do it in one compile
        printf("\n");
#endif /* GEN_FEEDBACK */
#else /* CODERUNNER */
	int index[numIndices]={0,1,2,3,4,5};	// solution for GL_TRIANGLE_STRIP
#endif
	glBegin(GL_TRIANGLE_STRIP);
	for(int i=0;i<numIndices;i++)
	{
		//glVertex2fv(vertices[index[i]]);
                glVertex3f(vertices[index[i]][0], vertices[index[i]][1], -0.01);
		printf("%d ",index[i]);
	}
	glEnd();



/*
	const int numIndices=6;	
	int index[numIndices]={0,1,2,3,4,5};	// solution for GL_QUAD_STRIP
	glBegin(GL_QUAD_STRIP);
	for(int i=0;i<numIndices;i++)
		glVertex2fv(vertices[index[i]]);
	glEnd();
*/
/*
	const int numIndices=8;	
	const int numColours=2;
	float colours[numColours][3]={{1.0, 0.0, 0.0}, {1.0, 1.0, 0.0}};
	int index[numIndices]={0,1,3,2,2,3,5,4};	// solution for GL_QUADS
	glBegin(GL_QUADS);
	for(int i=0;i<numIndices;i++)
		glColor3fv(colours[0]);
		glVertex2fv(vertices[index[i]]);
	glEnd();
*/

	// Draw the vertex numbers next to the points

	glPopMatrix();
	// start processing buffered OpenGL routines 
	glFlush ();
}

void init(void) 
{
	// select clearing color (for glClear)
	glClearColor (1.0, 1.0, 1.0, 0.0);	// RGB-value for white
        glClear(GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);

	// initialize view (simple orthographic projection)
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	GLdouble halfWidth=(GLdouble) windowWidth/2.0;
	GLdouble halfHeight=(GLdouble) windowHeight/2.0;
	gluOrtho2D(0,windowWidth,0,windowHeight);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

// create a single buffered colour window
int main(int argc, char** argv)
{
	glutInit(&argc, argv);		
#ifdef CODERUNNER
	CodeRunnerOpenGL codeRunnerOpenGL(windowWidth, windowHeight);
#else	
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(windowWidth, windowHeight); 
	glutInitWindowPosition(100, 100);
	glutCreateWindow("CompSci373 - OpenGL 2D Primitives");
#endif
	init ();						// initialise view
#ifdef CODERUNNER
	display();
#else
	glutDisplayFunc(display);		// draw scene
	glutMainLoop();
#endif

	return 0;
}
