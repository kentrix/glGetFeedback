#include "primitive_helper.h"
#include "drawPrimitives.hpp"
#include <assert.h>
#include <stdlib.h>
#include <png++/png.hpp>

#define PRINTED_OUTPUT 1
#define NOT_PRINTED_OUTPUT 0

#define TRUE 1
#define FALSE 0

int imageWriteCount = 0;

inline int odd(int i) {
    return i % 2;
}

void writeGLPixels(void) {
    png::image<png::rgba_pixel> buffer(windowWidth, windowHeight);
    uint8_t *buf = new uint8_t[windowWidth * windowHeight * 4];
    glReadPixels(-1, 0, windowWidth, windowHeight, GL_RGBA, GL_UNSIGNED_BYTE, buf);
    png::rgba_pixel *casted_buf = (png::rgba_pixel *)buf; 

    // writing to the libpng buffer and also flipping xy coords
    for (int y = 0; y < windowHeight; y++) {
        for (int x = 0; x < windowWidth; x++) {
            buffer[windowHeight - y - 1][x] = casted_buf[y * windowWidth + x];
        }
    }

    auto s = std::to_string(imageWriteCount++);
    auto ns = "image" + s + ".png";

    buffer.write(ns);

    delete [] buf;
}	

inline int is_array_reverse(int* a, int* b, int size) {
    for(int i = 0; i < size; i++) {
        if(*(a+i) != *(b+size-i-1)) return 0;
    }
    return 1;
}

inline int is_array_eq(int* a, int* b, int size) {
    for(int i = 0; i < size; i++) {
        if(*(a+i) != *(b+i)) return 0;
    }
    return 1;
}

typedef struct FourPair {
    int data[4];
    int checkedOff;
} FourPair;

typedef struct ThreePair {
    int data[3];
    int checkedOff;
} ThreePair;

int is_four_pair_equal(FourPair* pair, int* arr) {
    return is_array_eq(pair->data, arr, 4);
}

int is_three_pair_equal(ThreePair* pair, int* arr) {
    return is_array_eq(pair->data, arr, 3);
}

int is_four_pair_reverse(FourPair* pair, int* arr) {
    return is_array_reverse(pair->data, arr, 4);
}

int is_three_pair_reverse(ThreePair* pair, int* arr) {
    return is_array_reverse(pair->data, arr, 3);
}

int is_three_pair_reverse_p(ThreePair* pairA, ThreePair* pairB) {
    return is_array_reverse(pairA->data, pairB->data, 3);
}

int is_three_pair_equal_p(ThreePair* pairA, ThreePair* pairB) {
    return is_array_eq(pairA->data, pairB->data, 3);
}

int three_pair_all_checked_off(ThreePair* pairs, int size) {
    for(;size >= 0; size--, pairs++) {
        if(!pairs->checkedOff) return FALSE;
    }
    return TRUE;
}

int four_pair_all_checked_off(FourPair* pairs, int size) {
    for(;size >= 0; size--, pairs++) {
        if(!pairs->checkedOff) return FALSE;
    }
    return TRUE;
}

int check_off_three_pairs(ThreePair* pairs, int pair_size, int* student, int answer_size) {
    int student_pair_size = answer_size / 3;
    for(int i = 0; i < pair_size; i++) {
        for(int j = 0; j < student_pair_size; j++) {
            if(is_three_pair_reverse(pairs+i, student+j*3)) {
                printf("Check again, you might have drawn some vertices in the wrong direction.");
                return PRINTED_OUTPUT;
            }
            else if (is_three_pair_equal(pairs+i, student+j*3)) {
                (pairs+i)->checkedOff = 1;
            }
        }
    }
    return NOT_PRINTED_OUTPUT;
}

void check_off_triangle(int* model, int* student, int model_size, int answer_size) {
    // The model's size should be a multiple of 3
    assert(model_size > 0 && model_size % 3 == 0);
    if(model_size < answer_size - 2) {
        printf("Have you drawn too many vertices?");
        return;
    }
    // Split off into chunks of 3-pairs
    int i = model_size / 3;
    ThreePair* model_pair = (ThreePair*)calloc(i, sizeof(ThreePair));
    for(int j = 0; j < i; j++) {
        for(int k = 0; k < 3; k++) {
            (model_pair+j)->data[k] = model[j*3+k];
        }
    }

    if(check_off_three_pairs(model_pair, i, student, answer_size) == NOT_PRINTED_OUTPUT) {
        // No feedback is printed in the call, check checkoff state
        if(!three_pair_all_checked_off(model_pair, i)) {
            printf("You seemed to be missing some vertices, have you drawn duplicates?");
        }
    }

    free(model_pair);
}

int check_off_triangle_pairs(ThreePair* model, ThreePair* ans, int msize, int asize) {
    for(int i = 0; i < msize; i++) {
        for(int k = 0; k < asize; k++) {
            if(is_three_pair_reverse_p(model+i, ans+k)) {
                printf("Check again, you might have drawn some vertices in the wrong direction.");
                return PRINTED_OUTPUT;
            }
            else if(is_three_pair_equal_p(model+i, ans+k)) {
                model->checkedOff = TRUE;
            }
        }
    }
    return NOT_PRINTED_OUTPUT;
}

int breakdown_fan_calls(int* arr, int arr_size, void** buf) {
    if(arr_size < 3) {
        return 0;
    }
    int size = arr_size - 2;
    *buf = calloc(size, sizeof(ThreePair));
    ThreePair* iter = (ThreePair*)*buf;
    for(int i = 0; i < size; i++) {
        (iter+i)->data[0] = arr[0];
        (iter+i)->data[1] = arr[i+1];
        (iter+i)->data[2] = arr[i+2];
    }
    return size;
}

int breakdown_strip_calls(int* model, int model_size, int mode, void** buf) {
    int size;
    switch(mode) {
        case GL_TRIANGLE_STRIP:
            {
                // n-2 triangles
                size = model_size - 2;
                *buf = calloc(size, sizeof(ThreePair));
                ThreePair* iter = (ThreePair*)*buf;
                for(int i = 0; i < size; i++) {
                    if(odd(i)) {
                        (iter+i)->data[0] = model[i+1];
                        (iter+i)->data[1] = model[i];
                        (iter+i)->data[2] = model[i+2];
                    }
                    else {
                        for(int k = 0; k < 3; k++) {
                            (iter+i)->data[k] = model[i+k];
                        }
                    }
                }
                return size;
            }
        case GL_QUAD_STRIP: 
            {
            size = (model_size - 2) / 2;
            *buf = calloc(size, sizeof(FourPair));
            FourPair* iterf = (FourPair*)*buf;
            int cond = 0;
            for(int i = 0; i < size; i+=2, cond++) {
                if(odd(cond)) {
                    for(int k = 0; k < 4; k++) {
                        (iterf+cond)->data[k] = model[i+k];
                    }
                }
                else {
                    for(int k = 0; k < 4; k++) {
                        (iterf+cond)->data[k] = model[i+k];
                    }
                }
            }
            return size;
            }
        default:
            return 0;
    }
}

void check_off_triangle_strip(int* model, int* student, int model_size, int answer_size) {
    int smodel, sstudent;
    void *mptr, *sptr;
    mptr = sptr = NULL;
    smodel = breakdown_strip_calls(model, model_size, GL_TRIANGLE_STRIP, &mptr);
    sstudent = breakdown_strip_calls(student, answer_size, GL_TRIANGLE_STRIP, &sptr);
    check_off_triangle_pairs((ThreePair*)mptr, (ThreePair*)sptr, smodel, sstudent);
    free(mptr);
    free(sptr);
}

//XXX: Remember to run this thru valgrind!


void gen_feedback(int* model, int* student, int model_size, int answer_size, int mode) {
    unsigned short correct = FALSE;
    if(model_size == answer_size && is_array_eq(model, student, model_size)) {
        // Correct answer
        printf("Well done!");
        correct = TRUE;
    }
    else if(answer_size > 0 && *student != *model) {
        printf("The start index is not quite right. Please read the question description carefully.");
    }
    else if(model_size == answer_size && is_array_reverse(model, student, model_size)) {
        printf("The output image might look the same but have you drawn the vertices the wrong way around?");
    }
    if(model_size > answer_size) {
        printf("Have you made sure you have drawn enough vertices?");
    }

    float var = -0.001;

    switch(mode){
        case GL_TRIANGLES:
            if(!correct)
                check_off_triangle(model, student, model_size, answer_size);
            // Animation
            for(int i = 0; i < answer_size; i++) {
                glBegin(GL_TRIANGLES);
                glVertex3f(vertices[i][0], vertices[i][1], var);
                glVertex3f(vertices[i+1][0], vertices[i+1][1], var);
                glVertex3f(vertices[i+2][0], vertices[i+2][1], var);
                glFlush();
                glEnd();
                writeGLPixels();
            }
            break;
        case GL_TRIANGLE_STRIP:
            {
                // Print the feedback if we dont have the correct answer
                if(!correct)
                    check_off_triangle_strip(model, student, model_size, answer_size);
                // Animation
                void *sptr = NULL;
                int size = breakdown_strip_calls(student, answer_size, GL_TRIANGLE_STRIP, &sptr);
                ThreePair* pairs = (ThreePair*) sptr;
                for(int i = 0; i < size; i++) {
                    glBegin(GL_TRIANGLES);
                    glVertex3f(vertices[(pairs+i)->data[0]][0], vertices[(pairs+i)->data[0]][1], var);
                    glVertex3f(vertices[(pairs+i)->data[1]][0], vertices[(pairs+i)->data[1]][1], var);
                    glVertex3f(vertices[(pairs+i)->data[2]][0], vertices[(pairs+i)->data[2]][1], var);
                    glFlush();
                    glEnd();
                    writeGLPixels();
                }
                free(sptr);
            }
            break;
        case GL_TRIANGLE_FAN:
            {
                if(!correct)
                    // TODO
                    ;
                // Animation
                int draw_size = answer_size - 2;
                for(int i = 0; i < draw_size; i+=2) {
                    glBegin(GL_TRIANGLES);
                    glVertex3f(vertices[0][0], vertices[0][1], var);
                    glVertex3f(vertices[i+1][0], vertices[i+1][1], var);
                    glVertex3f(vertices[i+2][0], vertices[i+2][1], var);
                    glFlush();
                    glEnd();
                    writeGLPixels();
                }
            }
            break;
        case GL_QUADS:
            {
                if(!correct)
                    //TODO
                    ;
                // Animation
                int draw_size = answer_size / 4;
                for(int i = 0; i < draw_size; i+=4) {
                    glBegin(GL_QUADS);
                    glVertex3f(vertices[i][0], vertices[i][1], var);
                    glVertex3f(vertices[i+1][0], vertices[i+1][1], var);
                    glVertex3f(vertices[i+2][0], vertices[i+2][1], var);
                    glVertex3f(vertices[i+2][0], vertices[i+2][1], var);
                    glFlush();
                    glEnd();
                    writeGLPixels();
                }
            }
            break;
        case GL_QUAD_STRIP:
            {
                if(!correct)
                    //TODO
                    ;
                // Animation
                void *sptr = NULL;
                int size = breakdown_strip_calls(student, answer_size, GL_QUAD_STRIP, &sptr);
                FourPair* pairs = (FourPair*) sptr;
                for(int i = 0; i < size; i++) {
                    glBegin(GL_TRIANGLES);
                    glVertex3f(vertices[(pairs+i)->data[0]][0], vertices[(pairs+i)->data[0]][1], var);
                    glVertex3f(vertices[(pairs+i)->data[1]][0], vertices[(pairs+i)->data[1]][1], var);
                    glVertex3f(vertices[(pairs+i)->data[2]][0], vertices[(pairs+i)->data[2]][1], var);
                    glVertex3f(vertices[(pairs+i)->data[3]][0], vertices[(pairs+i)->data[3]][1], var);
                    glFlush();
                    glEnd();
                    writeGLPixels();
                }
                free(sptr);
            }
            break;
        default:
            printf("Invalid GL Paramemter, report this issue to your tutor.");
            break;
        }
   
}
