#include <stdio.h>
#include <GL/gl.h>

#ifndef PRIMITIVE_HELPER_H
#define PRIMITIVE_HELPER_H

void gen_feedback(int* model, int* student, int model_size, int student_size, int mode);

#endif
