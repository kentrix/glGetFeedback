#include "Color.h"

Color::Color()
{
}

Color::Color(float r, float g, float b)
{
	this->r = r;
	this->g = g;
	this->b = b;
}

Color::~Color(void)
{
}

Color Color::operator+(Color c)
{
	return Color(r + c.r, g + c.g, b + c.b);
}

Color Color::operator*(Color c)
{
	return Color(r * c.r, g * c.g, b * c.b);
}

Color Color::operator*(float s)
{
	return Color(s * r, s * g, s * b);
}
