class Color
{
public:
	Color();
	Color(float r, float g, float b);
	~Color(void);

	float r, g, b;

	Color operator+(Color c);
	Color operator*(Color c);
	Color operator*(float s);
};
