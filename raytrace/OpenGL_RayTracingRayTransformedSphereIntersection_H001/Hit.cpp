#include "Hit.h"

Hit::Hit(void)
{
}

Hit::Hit(Vector source, Vector d, float t, SceneObject* object)
{
	this->source = source;
	this->d = d;
	this->t = t;
	this->object = object;
}

Hit::~Hit(void)
{
}

Vector Hit::HitPoint()
{
	/*=======================================*/
	/*== Calculate the hit point here      ==*/
	/*=======================================*/
	return Vector(source.x + t*d.x, source.y + t*d.y, source.z + t*d.z);
}