#pragma once
#include "Vector.h"
#include "SceneObject.h"

class Hit
{
public:
	Hit(void);
	Hit(Vector source, Vector d, float t, SceneObject* object);
	~Hit(void);

	Vector source;
	Vector d;
	float t;
	SceneObject* object;

	Vector HitPoint();
};
