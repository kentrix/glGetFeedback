#pragma once
#include "Color.h"
#include "Vector.h"

class Light
{
public:
	Light(void);
	~Light(void);

	Vector position;
	Color ambient, diffuse, specular;
};
