#pragma once
#include "Vector.h"
#include "SceneObject.h"

class Plane : public SceneObject
{
public:
	Plane(void);
	~Plane(void);

	Vector n;  
	float a;  // distance from origin

	double Intersect(Vector source, Vector d);
	Vector Normal(Vector p);
};
