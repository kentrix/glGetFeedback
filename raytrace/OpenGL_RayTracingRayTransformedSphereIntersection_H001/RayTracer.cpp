// A basic OpenGL program illustrating RayTracing
// Burkhard Wuensche, 27 April 2017


#ifndef CODERUNNER
#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include <gl/glut.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif

#include <math.h>
#include <stdio.h>
#include "Color.h"
#include "Vector.h"
#include "Hit.h"
#include "SceneObject.h"
#include "Sphere.h"
#include "Plane.h"
#include "Light.h"

#ifdef CODERUNNER
#include "coderunneropengl.hpp"
#endif /* CODERUNNER */

int counter=0;		// counter to generate test output in intersect method

const int windowWidth = 300;
const int windowHeight = 300;



// objects
const int numObjects = 2;
SceneObject* objects[numObjects];

Color background = Color(0.5, 0.8, 1);

// lights
const int numLights = 2;
Light* lights[numLights];

// camera
Vector eye = Vector(0, 0, 4);
Vector u = Vector(1, 0, 0);
Vector v = Vector(0, 1, 0);
Vector n = Vector(0, 0, 1);
float N = 1.0, W = 0.5, H = 0.5;


void setupScene()
{
	Sphere* s = new Sphere();
	s->ambient = Color(0.1, 0.1, 0.1);
	s->diffuse = Color(1.0, 0.2, 0.2);
	s->specular = Color(0.7, 0.7, 0.7);
	s->shininess =  50;
	s->reflectivity = 0.4;
	objects[0] = s;
	s->translation = Vector(0.2,0.6,0);
	s->scaling = Vector(1.4,0.8,1);
//	s->translation = Vector(0,0,0);
//	s->scaling = Vector(1,1,1);


	Plane* p = new Plane();
	p->n = Vector(0, 1, 0);
	p->a = -1;
	p->ambient = Color(0.1, 0.1, 0.1);
	p->diffuse = Color(0.2, 0.5, 0.2);
	p->specular = Color(0.7, 0.7, 0.7);
	p->shininess =  50;
	p->reflectivity = 0.4;
	objects[1] = p;

	Light* l = new Light();
	l->position = Vector(-2, 2, 2);
	l->ambient = Color(0.1, 0.1, 0.1);
	l->diffuse = Color(0.5, 0.5, 0.5);
	l->specular = Color(0.5, 0.5, 0.5);
	lights[0] = l;

	l = new Light();
	l->position = Vector(3, 1, 3);
	l->ambient = Color(0.1, 0.1, 0.1);
	l->diffuse = Color(0.4, 0.4, 0.6);
	l->specular = Color(0.3, 0.3, 0.5);
	lights[1] = l;
}


Hit intersect(Vector source, Vector d)
{
	// initially hit object==NULL (no hit)
	Hit hit = Hit(source, d, -1, NULL);

	// for every object, check if ray hits it
	for(int i=0; i<numObjects; i++) {
		float t = objects[i]->Intersect(source, d);

		// 1. only use hits visible for the camera
		// 2. only overwrite hit if either there is 
		//     no hit yet, or the hit is closer
		if(t>0.00001 && (hit.object==NULL || t<hit.t))
			hit = Hit(source, d, t, objects[i]);
	}
	return hit;
}


Color shade(Hit hit)
{
	// if no object was hit, return background color
	if (hit.object == NULL) {

		// for coderunner
		if (counter++ % 10000 == 0)
			printf("color=(%.2f, %.2f, %.2f)\n", background.r, background.g, background.b);

		return background;
	}

	Color color = Color(0,0,0);
	
	for(int i=0; i<numLights; i++) {
		// ambient reflection
		color = color + hit.object->ambient * lights[i]->ambient;

		Vector p = hit.HitPoint();
		Vector v = hit.source - p;
		Vector s = lights[i]->position - p;
		Vector m = hit.object->Normal(p);

        // make sure light hits the front face
		if (s.Dot(m) < 0)
			continue;

		// cast a "shadow feeler"
		Hit feeler = intersect(p, s);
		if(feeler.object==NULL || feeler.t<0 || feeler.t>1) {
			// diffuse reflection
			color = color + hit.object->diffuse * lights[i]->diffuse * s.Normalize().Dot(m);

			// specular reflection
			Vector h = (s.Normalize() + v.Normalize()).Normalize();
			color = color + hit.object->specular * lights[i]->specular 
				*  pow(h.Dot(m), hit.object->shininess);
		}
	}

	// for coderunner
	if (counter++ % 10000 == 0)
		printf("color=(%.2f, %.2f, %.2f)\n", color.r, color.g, color.b);

	return color;
}


void display(void)
{
	double dx, dy, dz;

	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_POINTS);

	for(int r=0; r<windowHeight; r++) {
		for(int c=0; c<windowWidth; c++) {
			// construct ray through (c, r) using u,v,n and H,W

			// =================================================================================
			// == Construct the ray here                                                      ==
			// == Compute the ray direction (dx, dy, dz) by modifying the equations for       ==
			// ==         these values appropriately                                          ==
			// == Please use the following parameters:                                        ==
			// ==   W and H - window width and height  (in units)                             ==
			// ==   N - view plane distance from eye                                          ==
			// ==   windowWidth and windowHeight - size of display window in pixels)          ==
			// ==	                               (in lecture notes called nCols and nRows)  ==
			// ==   c and r - index of current pixel                                          ==
			// ==   u.x,u.y,u.z, v.x,v.y,v.z, n.x, n.y, n.z - uvn view coordinate system      ==
			// =================================================================================

			dx = -n.x*N + W*(2.0*c/(windowWidth-1)-1)*u.x + H*(2.0*r/(windowHeight-1)-1)*v.x;
			dy = -n.y*N + W*(2.0*c/(windowWidth-1)-1)*u.y + H*(2.0*r/(windowHeight-1)-1)*v.y;
			dz = -n.z*N + W*(2.0*c/(windowWidth-1)-1)*u.z + H*(2.0*r/(windowHeight-1)-1)*v.z;
			Vector d = Vector(dx, dy, dz);

			// intersect ray with scene objects
			Hit hit = intersect(eye, d);
	
			// shade pixel accordingly
			Color color = shade(hit);
			glColor3f(color.r, color.g, color.b);
			glVertex2f((GLfloat)c, (GLfloat)r);
		}
	}


	glEnd();
	glFlush();
}

void init(void)
{
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, windowWidth, 0, windowHeight);
	setupScene();
}



int main(int argc, char** argv)
{
#ifdef CODERUNNER
	CodeRunnerOpenGL codeRunnerOpenGL(windowWidth, windowHeight);
#else	
	glutInit(&argc, argv);		
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(windowWidth, windowHeight); 
	glutInitWindowPosition(100, 100);
	glutCreateWindow("OpenGL Ray Tracer");
#endif
	init ();								// initialise view
#ifdef CODERUNNER
	display();
#else
	glutDisplayFunc(display);		// draw scene
	glutMainLoop();
#endif

	return 0;
}
