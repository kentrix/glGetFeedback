#pragma once
#include "Color.h"
#include "Vector.h"

class SceneObject
{
public:
	Vector scaling, translation;
	Color ambient, diffuse, specular;
	float shininess;

	float reflectivity;

	// returns the t value of the closest ray-object intersection, or -1 otherwise
	virtual double Intersect(Vector source, Vector d) = 0;

	// returns the normal at the given point p
	// if p is not on the object the result is undefined
	virtual Vector Normal(Vector p) = 0;
};
