#include "Sphere.h"
#include <stdio.h>
#include <math.h>

Sphere::Sphere(void)
{
}

Sphere::~Sphere(void)
{
}

#ifdef CODERUNNER
#include "answer.hpp"
#else

double Sphere::Intersect(Vector source, Vector d)
{
	// Compute the intersection of a ray with a TRANSFORMED sphere
	// You can assume that for a transformed object the attributes 'scaling' and 'translation" are defined.
	// where (scaling.x,scaling.y,scaling.z) are the scale factors in x,y, and z direction 
	// and (translation.x,translation.y,translation.z) is the translation vector

	// STEP 1: Transform the ray
	source=(source-translation)/scaling;
	d=d/scaling;


	// STEP 2: Compute interscetion of transformed ray with sphere
	// A, B, and C are the parameters of quadriatic equation for finding the 
	// ray intersection parameter t (see "Ray Tracing" lecture notes)
	float A = d.Dot(d);
	float B = 2*source.Dot(d);
	float C = source.Dot(source) - 1;

	float t; // the parameter t for the closest intersection point or ray with the sphere. If no intersection t=-1.0

	// BEGIN SOLUTION RAY-SPHERE INTERSECTION
	// ================================================================
	// == Delete the line "t=-1.0;" and insert your solution instead ==
	// == NOTE 1: If there is no ray-shere intersection set t=-1.0   == 
	// == NOTE 2: Use C notation so that the code runs in Coderunner ==
	// == NOTE 3: You might want to use the sqrt() function          == 
	// ================================================================
	// BEFORE t=-1.0;
	if(B*B - 4*A*C <= 0) return t=-1;  // no hit

	float t1;
	if(B>0)   // for numerical precision
	      t1 = (-B - sqrt(B*B - 4*A*C)) / (2*A);
	else
	      t1 = (-B + sqrt(B*B - 4*A*C)) / (2*A);

	float t2 = C/(A*t1); // easier way to get t2
	
	if(t1<t2) t=t1;  // need only closer t
	else t=t2;
	// END SOLUTION RAY-SPHERE INTERSECTION

	return t;
}


Vector Sphere::Normal(Vector p)
{
	/*=======================================*/
	/*== Calculate the sphere normal here  ==*/
	/*=======================================*/
	// BEFORE: return p.Normalize
	// replace the existing code below and use the values 'scaling' and 'translation" to compute the normal of the transformed sphere
	Vector p_unitSphere=(p-translation)/scaling;
	Vector n=p_unitSphere; // for a unit sphere every point is its own normal
	n=n/scaling;
	return n.Normalize();
}
#endif