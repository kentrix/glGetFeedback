#pragma once

#include "SceneObject.h"

class Sphere : public SceneObject
{
public:
	Sphere(void);
	~Sphere(void);

	double Intersect(Vector source, Vector d);
	Vector Normal(Vector p);
};
