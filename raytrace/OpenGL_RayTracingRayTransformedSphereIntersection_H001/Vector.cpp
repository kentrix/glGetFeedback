#include "Vector.h"

Vector::Vector()
{
}

Vector::Vector(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

Vector::~Vector(void)
{
}

float Vector::Dot(Vector v)
{
	return x*v.x + y*v.y + z*v.z;
}

Vector Vector::operator+(Vector v)
{
	return Vector(x + v.x, y + v.y, z + v.z);
}

Vector Vector::operator-(Vector v)
{
	return Vector(x - v.x, y - v.y, z - v.z);
}

Vector Vector::operator*(float s)
{
	return Vector(x * s, y * s, z * s);
}

Vector Vector::operator/(Vector v)
{
	return Vector(x / v.x, y / v.y, z / v.z);
}

Vector Vector::Scale(float sx, float sy, float sz)
{
	return Vector(x * sx, y * sy, z * sz);
}

bool Vector::operator==(Vector v)
{
	return ((fabs(x-v.x) < 0.0001) && (fabs(y-v.y) < 0.0001) && (fabs(z-v.z) < 0.0001));
}

Vector Vector::Normalize()
{
	float l = sqrt(this->Dot(*this));
	return Vector(x / l, y / l, z / l);
}
