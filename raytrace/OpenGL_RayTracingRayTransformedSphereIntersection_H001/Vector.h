#pragma once
#include <cmath>

class Vector
{
public:
	Vector();
	Vector(float x, float y, float z);
	~Vector(void);

	float x, y, z;

	float Dot(Vector v); // dot product
	Vector operator+(Vector v);
	Vector operator-(Vector v);
	Vector operator*(float s);
	Vector operator/(Vector v);
	bool operator==(Vector v);
	Vector Scale(float sx, float sy, float sz);
	Vector Normalize();
};
