// A basic OpenGL program illustrating OpenGL transformations
// Burkhard Wuensche, 6 April 2017

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include "gl-matrix.h"

#include <iostream>
using namespace std;
#include <math.h>
#include "Lighting.h"
#include "Geometry.h"

#ifdef CODERUNNER
#include "coderunneropengl.hpp"
#endif /* CODERUNNER */

const int windowWidth=500;
const int windowHeight=500;

// material properties of surface (used if shading enabled)
GLfloat mat_specular[4];
GLfloat mat_ambient_and_diffuse[4];
GLfloat mat_shininess[1];

CLighting lighting;


inline void drawString(const char* s, void* font=GLUT_BITMAP_TIMES_ROMAN_24){
	while(*s!='\0') glutBitmapCharacter(font, *(s++));
}

void drawCoordinateSystem()
{
	glLineWidth(3.0);

	glColor3f(0,0,0);
	glBegin(GL_LINES);
	glVertex3f(0,0,0);
	glVertex3f(4.5,0,0);
	glEnd();
	glPushMatrix();
	glTranslatef(0,0,4.5);
	glutSolidCone(0.15, 0.4, 16, 1);
	glPopMatrix();

	glBegin(GL_LINES);
	glVertex3f(0,0,0);
	glVertex3f(0,4.5,0);
	glEnd();
	glPushMatrix();
	glRotatef(90,0,1,0);
	glTranslatef(0,0,4.5);
	glutSolidCone(0.15, 0.4, 16, 1);
	glPopMatrix();

	glBegin(GL_LINES);
	glVertex3f(0,0,0);
	glVertex3f(0,0,4.5);
	glEnd();
	glPushMatrix();
	glRotatef(-90,1,0,0);
	glTranslatef(0,0,4.5);
	glutSolidCone(0.15, 0.4, 16, 1);
	glPopMatrix();

	glLineWidth(1.0);
	int i;
	// grid for 1st quadrant of xy-plane (positive x, positive y)
	glBegin(GL_LINES);
	for(i=1;i<=4;i++){
		glVertex3f(0,i,0);
		glVertex3f(4,i,0);
		glVertex3f(i,0,0);
		glVertex3f(i,4,0);
	}
	glEnd();

	// grid for 1st quadrant of xz-plane (positive x, positive z)
	glBegin(GL_LINES);
	for(i=1;i<=4;i++){
		glVertex3f(0,0,i);
		glVertex3f(4,0,i);
		glVertex3f(i,0,0);
		glVertex3f(i,0,4);
	}
	glEnd();

	// grid for 1st quadrant of yz-plane (positive y, positive z)
	glBegin(GL_LINES);
	for(i=1;i<=4;i++){
		glVertex3f(0,0,i);
		glVertex3f(0,4,i);
		glVertex3f(0,i,0);
		glVertex3f(0,i,4);
	}
	glEnd();

	// Draw the vertex numbers next to the points
	glColor3f(0.0, 0.0, 0.0);	// black colour
	glRasterPos3f(4.8, 0.2, 0.0);
	char* s=new char[2]; drawString("x");
	glRasterPos3f(0.2, 4.8, -0.2);
	drawString("y");
	glRasterPos3f(-0.3, 0.2, 4.8);
	drawString("z");
}

#ifdef CODERUNNER
#include "answer.hpp"
#else
double HermiteBasis1(double t){ return t*t*(2*t-3)+1;} // B1(t) 
double HermiteBasis2(double t){ return (-2*t+3)*t*t;}  // B2(t) 
double HermiteBasis3(double t){ return t*(t*(t-2)+1);} // B3(t)
double HermiteBasis4(double t){ return t*t*(t-1);}     // B4(t)

vec3_t c(float t, vec3_t p1, vec3_t p4, vec3_t r1, vec3_t r4)
{
	vec3_t p=HermiteBasis1(t)*p1+HermiteBasis2(t)*p4+HermiteBasis3(t)*r1+HermiteBasis4(t)*r4;
	return p;
}
#endif

void drawParametricCurve(vec3_t p1, vec3_t p4, vec3_t r1, vec3_t r4)
{
	int numSegments=50;

	glLineWidth(3.0);
	glColor3f(1,0,0);

	vec3_t v;

	glBegin(GL_LINE_STRIP);
	for(int i=0;i<=numSegments;i++)
	{
		v=c((float) i/(float) numSegments,p1,p4,r1,r4);
		glVertex3f(v[0], v[1], v[2]);
	}	
	glEnd();
}

void printRounded(double t, vec3_t p1, vec3_t p4, vec3_t r1, vec3_t r4){
	vec3_t v=c(t,p1,p4,r1,r4);
	printf("c(%.2f)=(%.2f, %.2f, %.2f)\n",t,v[X],v[Y],v[Z]);
}

void display(void)
{
	glMatrixMode( GL_MODELVIEW );	// Set the view matrix ...
	glLoadIdentity();				// ... to identity.
    gluLookAt(0,0,25, 0.5,1.5,0, 0,1,0); // camera is on the z-axis

	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	// rotate whole scene to get default view
	glRotatef(-20, 0, 1, 0);
	glRotatef(20, 1, 0, 0);
	glRotatef(-20, 0, 0, 1);
	
	glLineWidth(2.0);
	drawCoordinateSystem();

//	lighting.enable();

	double s=0.0f,t=0.0f;

        vec3_t p1, p4, r1, r4;
        p1 = (vec3_t)calloc(sizeof(double_t), 3);
        p4 = (vec3_t)calloc(sizeof(double_t), 3);
        r1 = (vec3_t)calloc(sizeof(double_t), 3);
        r4 = (vec3_t)calloc(sizeof(double_t), 3);

        vec3_set_value(p1, 3, 4, 0);
        vec3_set_value(p4, 0, 0, 2);
        vec3_set_value(r1, 6, 0, 0);
        vec3_set_value(r4, 0, 0, -12);

#ifdef CODERUNNER
#include "testcode.hpp"
#endif
        /*
	printRounded(0,p1,p4,r1,r4);
	printRounded(0.5,p1,p4,r1,r4);
	printRounded(1.0,p1,p4,r1,r4);
        */

	// draw parametric surface
	drawParametricCurve(p1,p4,r1,r4);

//	lighting.disable();
	glFlush ();
}

void init(void) 
{
	// select clearing color (for glClear)
	glClearColor (1,1,1,1);	// RGB-value for black
	// enable depth buffering
	glEnable(GL_DEPTH_TEST);
	// initialize view (simple orthographic projection)
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(25,1,10,30);
	// init lighting
	lighting.init();

	// material properties of the object
	mat_ambient_and_diffuse[0]=0.8;		// red material ...
	mat_ambient_and_diffuse[1]=0;
	mat_ambient_and_diffuse[2]=0;
	mat_ambient_and_diffuse[3]=1;
	mat_specular[0]=0.5f;				// ... with white highlights
	mat_specular[1]=0.5f;				// if light source is reflected
	mat_specular[2]=0.5f;				// on the material surface.
	mat_specular[3]=1;
	mat_shininess[0]=100;
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat_ambient_and_diffuse);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);

}



// create a double buffered colour window
int main(int argc, char** argv)
{
#ifdef CODERUNNER
	CodeRunnerOpenGL codeRunnerOpenGL(windowWidth, windowHeight);
#else	
	glutInit(&argc, argv);		
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(windowWidth, windowHeight); 
	glutInitWindowPosition(100, 100);
	glutCreateWindow("OpenGL Transformation");
#endif
	init ();								// initialise view
#ifdef CODERUNNER
	display();
#else
	glutDisplayFunc(display);		// draw scene
	glutMainLoop();
#endif

	return 0;
}
