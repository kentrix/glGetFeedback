
double BezierBasis1(double t){ t=1-t; return t*t*t;}   // B1(t)
double BezierBasis2(double t){ return 3*t*(1-t)*(1-t);}// B2(t)
double BezierBasis3(double t){ return 3*t*t*(1-t);}	    // B3(t)
double BezierBasis4(double t){ return t*t*t;}	    // B4(t)

CVec3df c(float t, CVec3df p1, CVec3df p2, CVec3df p3, CVec3df p4)
{
	CVec3df p=BezierBasis1(t)*p1+BezierBasis2(t)*p2+BezierBasis3(t)*p3+BezierBasis4(t)*p4;
	return p;
}
