// A basic OpenGL program illustrating Bezier curves
// Burkhard Wuensche, 6 April 2017

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <iostream>
using namespace std;
#include <math.h>
#include "Lighting.h"
#include "Geometry.h"

#ifdef CODERUNNER
#include "coderunneropengl.hpp"
#endif /* CODERUNNER */


const int windowWidth=500;
const int windowHeight=500;
const int numSegments=50;
const float tolerance = 0.001;

// material properties of surface (used if shading enabled)
GLfloat mat_specular[4];
GLfloat mat_ambient_and_diffuse[4];
GLfloat mat_shininess[1];

CLighting lighting;

string correctnessString;

#ifdef GEN_FEEDBACK
string feedbackString = "";
#endif

#ifdef JSON_DUMP
string dumpString = "";
#endif

inline int withinTolerance(double answer, double model, double tolerance) {
    if(isnan(answer) && isnan(model)) return 1;
    return model + tolerance > answer && model - tolerance < answer;
}

inline void drawString(const char* s, void* font=GLUT_BITMAP_TIMES_ROMAN_24){
	while(*s!='\0') glutBitmapCharacter(font, *(s++));
}

void drawCoordinateSystem()
{
	glLineWidth(3.0);

	glColor3f(0,0,0);
	glBegin(GL_LINES);
	glVertex3f(0,0,0);
	glVertex3f(4.5,0,0);
	glEnd();
	glPushMatrix();
	glTranslatef(0,0,4.5);
	glutSolidCone(0.15, 0.4, 16, 1);
	glPopMatrix();

	glBegin(GL_LINES);
	glVertex3f(0,0,0);
	glVertex3f(0,4.5,0);
	glEnd();
	glPushMatrix();
	glRotatef(90,0,1,0);
	glTranslatef(0,0,4.5);
	glutSolidCone(0.15, 0.4, 16, 1);
	glPopMatrix();

	glBegin(GL_LINES);
	glVertex3f(0,0,0);
	glVertex3f(0,0,4.5);
	glEnd();
	glPushMatrix();
	glRotatef(-90,1,0,0);
	glTranslatef(0,0,4.5);
	glutSolidCone(0.15, 0.4, 16, 1);
	glPopMatrix();

	glLineWidth(1.0);
	int i;
	// grid for 1st quadrant of xy-plane (positive x, positive y)
	glBegin(GL_LINES);
	for(i=1;i<=4;i++){
		glVertex3f(0,i,0);
		glVertex3f(4,i,0);
		glVertex3f(i,0,0);
		glVertex3f(i,4,0);
	}
	glEnd();

	// grid for 1st quadrant of xz-plane (positive x, positive z)
	glBegin(GL_LINES);
	for(i=1;i<=4;i++){
		glVertex3f(0,0,i);
		glVertex3f(4,0,i);
		glVertex3f(i,0,0);
		glVertex3f(i,0,4);
	}
	glEnd();

	// grid for 1st quadrant of yz-plane (positive y, positive z)
	glBegin(GL_LINES);
	for(i=1;i<=4;i++){
		glVertex3f(0,0,i);
		glVertex3f(0,4,i);
		glVertex3f(0,i,0);
		glVertex3f(0,i,4);
	}
	glEnd();

	// Draw the vertex numbers next to the points
	glColor3f(0.0, 0.0, 0.0);	// black colour
	glRasterPos3f(4.8, 0.2, 0.0);
	char* s=new char[2]; drawString("x");
	glRasterPos3f(0.2, 4.8, -0.2);
	drawString("y");
	glRasterPos3f(-0.3, 0.2, 4.8);
	drawString("z");
}

CVec3df c__MODEL__(float t, CVec3df p1, CVec3df p2, CVec3df p3, CVec3df p4)
{
    float a = 1-t;
    CVec3df p = 
        a*a*a*p1
        +3*t*a*a*p2
        +3*t*t*a*p3
        +t*t*t*p4;
    return p;
}

#ifdef CODERUNNER
#include "answer.hpp"
#else
double BezierBasis1(double t){ t=1-t; return t*t*t;}   // B1(t)
double BezierBasis2(double t){ return 3*t*(1-t)*(1-t);}// B2(t)
double BezierBasis3(double t){ return 3*t*t*(1-t);}	    // B3(t)
double BezierBasis4(double t){ return t*t*t;}	    // B4(t)

CVec3df c(float t, CVec3df p1, CVec3df p2, CVec3df p3, CVec3df p4)
{
	CVec3df p=BezierBasis1(t)*p1+BezierBasis2(t)*p2+BezierBasis3(t)*p3+BezierBasis4(t)*p4;
	return p;
}
#endif

void compareAnswerAndModel(CVec3df p1,CVec3df p2,CVec3df p3,CVec3df p4) {
    CVec3df ans, model;
    bool x, y, z;
    x = y = z = true;
    for(int i=0;i<=numSegments;i++) {
        ans = c((float) i/(float) numSegments, p1, p2, p3, p4);
        model = c__MODEL__((float) i/(float) numSegments, p1, p2, p3, p4);
        if(!withinTolerance(ans[0], model[0], tolerance)) {
            x = false;
        } else if(!withinTolerance(ans[1], model[1], tolerance)) {
            y = false;
        } else if(!withinTolerance(ans[2], model[2], tolerance)) {
            z = false;
        }
    }

#ifdef GEN_FEEDBACK
    if(!x) feedbackString += "Your X value is off, check again!\t";
    if(!y) feedbackString += "Your Y value is off, check again!\t";
    if(!z) feedbackString += "Your Z value is off, check again!\t";
#endif
    if(x && y && z) correctnessString = "1"; else correctnessString = "0";
}


void drawParametricCurve(CVec3df p1, CVec3df p2, CVec3df p3, CVec3df p4)
{

	glLineWidth(3.0);
	glColor3f(1,0,0);

	CVec3df v;

	glBegin(GL_LINE_STRIP);
	for(int i=0;i<=numSegments;i++)
	{
		v=c((float) i/(float) numSegments,p1,p2,p3,p4);
		glVertex3fv(v.getArray());
	}	
	glEnd();
}

void printRounded(double t, CVec3df p1, CVec3df p2, CVec3df p3, CVec3df p4){
	CVec3df v=c(t,p1,p2,p3,p4);
	printf("c(%.2f)=(%.2f, %.2f, %.2f)\n",t,v[X],v[Y],v[Z]);
}

void display(void)
{
	glMatrixMode( GL_MODELVIEW );	// Set the view matrix ...
	glLoadIdentity();				// ... to identity.
    gluLookAt(0,0,25, 0.5,1.5,0, 0,1,0); // camera is on the z-axis

	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	// rotate whole scene to get default view
	glRotatef(-20, 0, 1, 0);
	glRotatef(20, 1, 0, 0);
	glRotatef(-20, 0, 0, 1);
	
	glLineWidth(2.0);
	drawCoordinateSystem();

//	lighting.enable();

	double s=0.0f,t=0.0f;

	CVec3df p1(1,0,0);
	CVec3df p2(-2,5,0);
	CVec3df p3(4,3,0);
	CVec3df p4(3,0,0);

        p1.setVector(3,4,0);
        p2.setVector(3,4,4);
        p3.setVector(0,4,4);
        p4.setVector(0,0,0);

#ifdef CODERUNNER
//#include "testcode.hpp"
#endif
/*
	printRounded(0,p1,p2,p3,p4);
	printRounded(0.5,p1,p2,p3,p4);
	printRounded(1.0,p1,p2,p3,p4);
*/

	// draw parametric surface
	drawParametricCurve(p1,p2,p3,p4);
        compareAnswerAndModel(p1,p2,p3,p4);

//	lighting.disable();
	glFlush ();
#ifdef GEN_FEEDBACK
        cout << feedbackString << endl;
#else
        cout << endl;
#endif
        cout << correctnessString << endl;
#ifdef JSON_DUMP
        cout << dumpString << endl;
#endif
}

void init(void) 
{
	// select clearing color (for glClear)
	glClearColor (1,1,1,1);	// RGB-value for black
	// enable depth buffering
	glEnable(GL_DEPTH_TEST);
	// initialize view (simple orthographic projection)
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(25,1,10,30);
	// init lighting
	lighting.init();

	// material properties of the object
	mat_ambient_and_diffuse[0]=0.8;		// red material ...
	mat_ambient_and_diffuse[1]=0;
	mat_ambient_and_diffuse[2]=0;
	mat_ambient_and_diffuse[3]=1;
	mat_specular[0]=0.5f;				// ... with white highlights
	mat_specular[1]=0.5f;				// if light source is reflected
	mat_specular[2]=0.5f;				// on the material surface.
	mat_specular[3]=1;
	mat_shininess[0]=100;
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat_ambient_and_diffuse);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);

}



// create a double buffered colour window
int main(int argc, char** argv)
{
#ifdef CODERUNNER
	CodeRunnerOpenGL codeRunnerOpenGL(windowWidth, windowHeight);
	glutInit(&argc, argv);		
	glutCreateWindow("OpenGL Transformation");
#else	
	glutInit(&argc, argv);		
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(windowWidth, windowHeight); 
	glutInitWindowPosition(100, 100);
	glutCreateWindow("OpenGL Transformation");
#endif
	init ();								// initialise view
#ifdef CODERUNNER
	display();
#else
	glutDisplayFunc(display);		// draw scene
	glutMainLoop();
#endif

	return 0;
}
