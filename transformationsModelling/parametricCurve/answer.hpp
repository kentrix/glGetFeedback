CVec3df c(float t)
{
//	return CVec3df(3*cos(0.5*Pi*t), 3*sin(0.5*Pi*t), 0);  // Question M001
//	return CVec3df(3*cos(Pi*t), 3*sin(Pi*t), 0);  // Question M002
//	return CVec3df(4*cos(0.5*Pi*t), 0, 2*sin(0.5*Pi*t));  // Question M003
	return CVec3df(cos(1.5*Pi*t), 0, 2*sin(1.5*Pi*t));  // Question M004
}
