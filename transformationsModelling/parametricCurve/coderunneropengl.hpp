#ifndef CODERUNNEROPENGL_HPP_
#define CODERUNNEROPENGL_HPP_

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <GL/osmesa.h>
#include <iostream>
#include <string>
#include "png++/png.hpp"

class CodeRunnerOpenGL {
private:
	int windowWidth;
	int windowHeight;	
        int imageWriteCount = 0;

	OSMesaContext ctx;
	GLubyte *glBuffer;

	void writeGLPixels(void) const {
		png::image<png::rgba_pixel> buffer(windowWidth, windowHeight);
		uint8_t *buf = new uint8_t[windowWidth * windowHeight * 4];
		glReadPixels(-1, 0, windowWidth, windowHeight, GL_RGBA, GL_UNSIGNED_BYTE, buf);
		png::rgba_pixel *casted_buf = (png::rgba_pixel *)buf; 

		// writing to the libpng buffer and also flipping xy coords
		for (int y = 0; y < windowHeight; y++) {
			for (int x = 0; x < windowWidth; x++) {
				buffer[windowHeight - y - 1][x] = casted_buf[y * windowWidth + x];
			}
		}

		buffer.write("image.png");

		delete [] buf;
	}	


public:
	CodeRunnerOpenGL(int width, int height) : 
			windowWidth(width), windowHeight(height) {

		assert(sizeof(GLfloat) == sizeof(float));
		assert(sizeof(GLfloat) == 4);

		/* Create an RGBA-mode context */
#if OSMESA_MAJOR_VERSION * 100 + OSMESA_MINOR_VERSION >= 305
   		/* specify Z, stencil, accum sizes */
		ctx = OSMesaCreateContextExt( OSMESA_RGBA, 16, 0, 0, NULL );
#else
		ctx = OSMesaCreateContext( OSMESA_RGBA, NULL );
#endif
		if (!ctx) {
			std::cerr << "OSMesaCreateContext failed!" << std::endl;
			abort();
		}

		/* Allocate the image buffer */
		glBuffer = (GLubyte *)malloc( width * height * 4 * sizeof(GLubyte) );
		if (!glBuffer) {
			std::cerr << "Alloc image buffer failed!" << std::endl;
			abort();
		}

		/* Bind the buffer to the context and make it current */
		if (!OSMesaMakeCurrent( ctx, glBuffer, GL_UNSIGNED_BYTE, width, height )) {
			std::cerr << "OSMesaMakeCurrent failed!" << std::endl;
			abort();
		}
	}



	virtual ~CodeRunnerOpenGL(void) {
		writeGLPixels();
	}
};



#endif


