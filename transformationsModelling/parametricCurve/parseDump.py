#!/bin/env python
import os, sys, re
import json

theLine = 'glBegin(mode = GL_LINE_STRIP)'
theEndLine = 'glEnd()'
jsonOut = 'points.json'
floatingPointPattern = r'[+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?'
glVertexPattern = r'glVertex3fv\(v = \{(?P<x>FP), (?P<y>FP), (?P<z>FP)\}\)'
glVertexPattern = glVertexPattern.replace('FP', floatingPointPattern);

def main():
    fileIn = sys.argv[0]
    dump = open(fileIn, 'r')
    lines = dump.readlines()
    drawingCode = []
    for (i, l) in enumerate(lines):
        match = l.find(theLine)
        if match is not -1:
            rest = removeLineNum(l)
            for nl in lines[i+1:]:
                n = nl.find(theEndLine)
                if n is -1:
                    drawingCode.append(removeLineNum(nl).strip())
                else:
                    break

    out = []
    for d in drawingCode:
        out.append(re.search(glVertexPattern, d).groupdict())
    with open(jsonOut, 'w') as outfile:
        outfile.write(json.dumps(out))


def removeLineNum(s):
    return s[s.find(' ')+1:]
    

if __name__ == '__main__':
    main()
