// Geometry.h: interface for the Geometry class.
// Copyright: (c) Burkhard Wuensche, 1997
//////////////////////////////////////////////////////////////////////

#include <math.h>
#if !defined(LECTURE_372FC_GEOMETRY)
#define LECTURE_372FC_GEOMETRY

#include <iostream>
using namespace std;
#include "Utilities.h"

typedef enum { X, Y, Z, W } Axis;	// includes 3D homogenous coordinates

#define ZEROTOL 1.0e-30
const float Eps = 1.0e-5f;
const float Pi = 3.14159265358979323846264338327f;
const float Euler = 2.71828182845904523536028747135f;

//int pow(int a, int b);

// 3D double vector
class CVec3df {
	public:
	    // some good friends
	    friend class CMatrix3df;

	    // Constructors/ Destructor
	    CVec3df();
	    CVec3df(float x, float y, float z);
		CVec3df(const CVec3df& v); // Copy constructor
		virtual ~CVec3df();

	    // Assignment operator
	    CVec3df& operator=(const CVec3df& v1);

		// Vector in array form
		float* getArray() { return v;};
		void getArray(float *array) { array[0]=v[0]; array[1]=v[1]; array[2]=v[2];};
		void setArray(float *array) { v[0]=array[0]; v[1]=array[1]; v[2]=array[2];};
		void getFloatArray(float *array) { array[0]=(float) v[0]; array[1]=(float) v[1]; array[2]=(float) v[2];};

	    // some other operators
	    float& operator[](int index) const { return v[index]; }
	    CVec3df& operator+=(const CVec3df& v1);
	    CVec3df& operator-=(const CVec3df& v1);
	    CVec3df& operator*=(float scalar);
	    CVec3df& operator/=(float scalar);
		void setVector(float v0, float v1, float v2) {v[0]=v0; v[1]=v1; v[2]=v2;}
		void getVector(float& v0, float& v1, float& v2) {v0=v[0]; v1=v[1]; v2=v[2];}
		void setVector(int axis, float value) {v[axis]=value;}
		void getVector(int axis, float& value) {value=v[axis];}

	    friend CVec3df operator+(const CVec3df& v1, const CVec3df& v2);
	    friend CVec3df operator-(const CVec3df& v1, const CVec3df& v2);
	    friend CVec3df operator*(float scalar, const CVec3df& v1);
	    friend CVec3df operator*(const CVec3df& v1, float scalar);
	    friend CVec3df operator/(const CVec3df& v1, float scalar);
	    friend CVec3df operator*(const CVec3df& v1, const CVec3df& v2);
	    friend CVec3df operator-(const CVec3df& v1);
	    friend bool operator==(const CVec3df& v1, const CVec3df& v2);
	    friend bool operator!=(const CVec3df& v1, const CVec3df& v2);

	    // I/O operator
	    friend ostream& operator<<(ostream& s, const CVec3df& v1);
	    friend istream& operator>>(istream& s, CVec3df& v1);

	    // length
	    float length(void) const;

	    // normalize
	    CVec3df normalise(void) const;		// returns a normalised vector
		void normaliseDestructive(void);	// normalises the calling vector object
		void normaliseDestructiveNoError(void);	// normalises the calling vector object 
												// no error if vector has length zero
	    // dot product
	    float dot(const CVec3df& v1) const;

	    // cross product
	    CVec3df cross(const CVec3df& v1) const;

		// rotate angle degree around axis i (0 or X -> x-axis, 1 or Y -> y-axis, 2 or Z -> z-axis)
		CVec3df rotate(const int i, float angle) const; 

		// reflect v1 on the calling vector
		CVec3df reflect(const CVec3df& v1) const;

		// decomposes the calling vector into two components linear and orthogonal to n
		void decompose(const CVec3df& n, CVec3df& vLinear, CVec3df& vOrthogonal);
	private:
	    float* v;
};

// more convienent way to use the dot and cross products
inline float dot(const CVec3df& v1, const CVec3df& v2) { return v1.dot(v2); } 
inline CVec3df cross(const CVec3df& v1, const CVec3df& v2) { return v1.cross(v2); } 
inline CVec3df fabs(const CVec3df& v) { return CVec3df((float) fabs(v[X]), (float) fabs(v[Y]), (float) fabs(v[Z])); }
inline float length(const CVec3df& v) { return v.length();}
inline CVec3df reflect(const CVec3df& v, const CVec3df& n) { return n.reflect(v);}

// Some useful vectors (zero vector and 3D cartesian basis vectors)
const CVec3df ZeroVector3df = CVec3df(0.0, 0.0, 0.0);
const CVec3df E1_3df = CVec3df(1.0, 0.0, 0.0);
const CVec3df E2_3df = CVec3df(0.0, 1.0, 0.0);
const CVec3df E3_3df = CVec3df(0.0, 0.0, 1.0);


// 3D float Matrix
class CMatrix3df {
	public:
	    // Constructors/ Destructor
	    CMatrix3df();
	    CMatrix3df(float a11, float a12, float a13,
		   float a21, float a22, float a23,
		   float a31, float a32, float a33);
	    CMatrix3df(const CVec3df& v1,const CVec3df& v2,const CVec3df& v3);
	    CMatrix3df(const CMatrix3df& m1); // Copy constructor
		virtual ~CMatrix3df();

	    // Assignment operator
	    CMatrix3df& operator=(const CMatrix3df& m1);

	    // some other operators
	    CVec3df operator[](const int i) const { return CVec3df(m[i],m[3+i],m[6+i]); }
	    float& operator()(int i, int j) const { return m[3*i+j]; }
	    CMatrix3df& operator+=(const CMatrix3df& m1);
	    CMatrix3df& operator-=(const CMatrix3df& m1);
	    CMatrix3df& operator*=(const CMatrix3df& m1);
	    CMatrix3df& operator*=(float scalar);
	    CMatrix3df& operator/=(float scalar);
	    friend CMatrix3df operator*(float scalar, const CMatrix3df& m1);
	    friend CMatrix3df operator*(const CMatrix3df& m1, float scalar);
	    friend CMatrix3df operator/(const CMatrix3df& m1, float scalar);
	    friend CVec3df operator*(const CMatrix3df& m1, const CVec3df& v1);
	    friend CVec3df operator*(const CVec3df& v1, const CMatrix3df& m1);
	    friend CMatrix3df operator+(const CMatrix3df& m1, const CMatrix3df& m2);
	    friend CMatrix3df operator*(const CMatrix3df& m1, const CMatrix3df& m2);

	    // I/O operator
	    friend ostream& operator<<(ostream& s, const CMatrix3df& m1);
	    friend istream& operator>>(istream& s, CMatrix3df& m1);

	    // calculation of the transposed CMatrix
	    CMatrix3df transpose(void) const;

	    // calculation of the inverse CMatrix
//	    CMatrix3df inverse(void) const;

	    // calculation of the determinante
	    float det(void) const;

	private:
		float* m;	// matrix stored in a 1D array row wise
};

// more convienent way to use the transpose method
inline CMatrix3df transpose(CMatrix3df& m1) { return m1.transpose(); }
// more convienent way to obtain a normalised vector
inline CVec3df normalise(CVec3df& v) { return v.normalise(); }

// some useful matrices (zero matrix and identityMatrix
const CMatrix3df M0_3df = CMatrix3df(ZeroVector3df, ZeroVector3df, ZeroVector3df);
const CMatrix3df M1_3df = CMatrix3df(E1_3df, E2_3df, E3_3df);

#endif // !defined(LECTURE_372FC_GEOMETRY)
