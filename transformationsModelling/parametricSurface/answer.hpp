CVec3df p(float s, float t)
{
        return CVec3df((1-s)*cos(0.5*Pi*t), 2*(1-s)*sin(0.5*Pi*t), 4*s);
}

