
CVec3df c(float t)
{
	return CVec3df(3*sin(0.5*Pi*t), 0, 2*cos(0.5*Pi*t));
}
