// A basic OpenGL program illustrating OpenGL Surfaces of Revolution
// Burkhard Wuensche, 18 April 2017

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <iostream>
using namespace std;
#include <math.h>
#include "Lighting.h"
#include "Geometry.h"

#ifdef CODERUNNER
#include "coderunneropengl.hpp"
#endif /* CODERUNNER */

const int windowWidth=500;
const int windowHeight=500;
const int numSegmentsS=30;
const int numSegmentsT=30;


// material properties of surface (used if shading enabled)
GLfloat mat_specular[4];
GLfloat mat_ambient_and_diffuse[4];
GLfloat mat_shininess[1];
const float tolerance = 0.001f;

CLighting lighting;

string correctnessString;

#ifdef GEN_FEEDBACK
string feedbackString = "";
#endif

#ifdef JSON_DUMP
string dumpString = "";
#endif

inline int withinTolerance(double answer, double model, double tolerance) {
    if(isnan(answer) && isnan(model)) return 1;
    return model + tolerance > answer && model - tolerance < answer;
}


inline void drawString(const char* s, void* font=GLUT_BITMAP_TIMES_ROMAN_24){
	while(*s!='\0') glutBitmapCharacter(font, *(s++));
}

void drawCoordinateSystem()
{
	glLineWidth(3.0);

	glColor3f(0,0,0);
	glBegin(GL_LINES);
	glVertex3f(0,0,0);
	glVertex3f(4.5,0,0);
	glEnd();
	glPushMatrix();
	glTranslatef(0,0,4.5);
	glutSolidCone(0.15, 0.4, 16, 1);
	glPopMatrix();

	glBegin(GL_LINES);
	glVertex3f(0,0,0);
	glVertex3f(0,4.5,0);
	glEnd();
	glPushMatrix();
	glRotatef(90,0,1,0);
	glTranslatef(0,0,4.5);
	glutSolidCone(0.15, 0.4, 16, 1);
	glPopMatrix();

	glBegin(GL_LINES);
	glVertex3f(0,0,0);
	glVertex3f(0,0,4.5);
	glEnd();
	glPushMatrix();
	glRotatef(-90,1,0,0);
	glTranslatef(0,0,4.5);
	glutSolidCone(0.15, 0.4, 16, 1);
	glPopMatrix();

	glLineWidth(1.0);
	int i;
	// grid for 1st quadrant of xy-plane (positive x, positive y)
	glBegin(GL_LINES);
	for(i=1;i<=4;i++){
		glVertex3f(0,i,0);
		glVertex3f(4,i,0);
		glVertex3f(i,0,0);
		glVertex3f(i,4,0);
	}
	glEnd();

	// grid for 1st quadrant of xz-plane (positive x, positive z)
	glBegin(GL_LINES);
	for(i=1;i<=4;i++){
		glVertex3f(0,0,i);
		glVertex3f(4,0,i);
		glVertex3f(i,0,0);
		glVertex3f(i,0,4);
	}
	glEnd();

	// grid for 1st quadrant of yz-plane (positive y, positive z)
	glBegin(GL_LINES);
	for(i=1;i<=4;i++){
		glVertex3f(0,0,i);
		glVertex3f(0,4,i);
		glVertex3f(0,i,0);
		glVertex3f(0,i,4);
	}
	glEnd();

	// Draw the vertex numbers next to the points
	glColor3f(0.0, 0.0, 0.0);	// black colour
	glRasterPos3f(4.8, 0.2, 0.0);
	char* s=new char[2]; drawString("x");
	glRasterPos3f(0.2, 4.8, -0.2);
	drawString("y");
	glRasterPos3f(-0.3, 0.2, 4.8);
	drawString("z");
}

CVec3df c__MODEL__(float t)
{
	return CVec3df(3*sin(0.5*Pi*t), 0, 2*cos(0.5*Pi*t));
}

#ifdef CODERUNNER
#include "answer.hpp"
#else
CVec3df c(float t)
{
	return CVec3df(3*sin(0.5*Pi*t), 0, 2*cos(0.5*Pi*t));
}
#endif

void compareAnswerAndModel() {
    CVec3df ans, model;
    bool x, y, z;
    float s, t;
    x = y = z = true;
    for(int i = 0; i <= numSegmentsS; i++) {
        for(int j = 0; j <= numSegmentsT; j++) {
            s = (float) i/(float) numSegmentsS;
            t = (float) j/(float) numSegmentsT;
            ans = c(s);
            model = c__MODEL__(s);
            if(!withinTolerance(ans[0], model[0], tolerance)) {
                x = false;
            } else if(!withinTolerance(ans[1], model[1], tolerance)) {
                y = false;
            } else if(!withinTolerance(ans[2], model[2], tolerance)) {
                z = false;
            }
        }
    }
#ifdef GEN_FEEDBACK
    if(!x) feedbackString += "Your X value is off, check again!\t";
    if(!y) feedbackString += "Your Y value is off, check again!\t";
    if(!z) feedbackString += "Your Z value is off, check again!\t";
#endif
    if(x && y && z) correctnessString = "1"; else correctnessString = "0";
}

void drawProfileCurve()
{
	glColor3f(0,0,1);
	glLineWidth(3.0);

	int numSegments=30;
	glBegin(GL_LINE_STRIP);
	for(int i=0;i<=numSegments;i++)
		glVertex3fv(c((float) i/(float) numSegments).getArray());
	glEnd();
}

void drawSurfaceOfRevolution()
{
	glEnable(GL_NORMALIZE);
	int i,j;
	double s,t,ds,dt;
	CVec3df p0,p1,p2,dpdt,dpds,n,c0,c1,c2;
#ifdef JSON_DUMP
        dumpString += "[";
#endif

	for(i=0;i<numSegmentsS;i++)
	{
		glBegin(GL_QUAD_STRIP);
		for(j=0;j<=numSegmentsT;j++)
		{	
			s=(float) i/(float) numSegmentsS;
			t=(float) j/(float) numSegmentsT;
			dt=1.0f/(float) numSegmentsT;
			ds=1.0f/(float) numSegmentsS;
			c0=c(t);
			p0.setVector(c0[X]*cos(2*Pi*s), c0[X]*sin(2*Pi*s),c0[Z]);
			c1=c(t+dt);
			p1.setVector(c1[X]*cos(2*Pi*s), c1[X]*sin(2*Pi*s),c1[Z]);
			dpdt=(1.0f/dt)*(p1-p0);
			p2.setVector(c0[X]*cos(2*Pi*(s+ds)), c0[X]*sin(2*Pi*(s+ds)),c0[Z]);
			dpds=(1.0f/ds)*(p2-p0);
			n=cross(dpdt,dpds); 
			n.normaliseDestructiveNoError();
			glNormal3fv(n.getArray());
			glVertex3fv(p0.getArray());
#ifdef JSON_DUMP
                        dumpString += "{\"x\":" + to_string(p0[0]) + "," + \
                                       "\"y\":" + to_string(p0[1]) + "," + \
                                       "\"z\":" + to_string(p0[2]) + "}";
                        dumpString += ",";
#endif

			p0=p2;
			p1.setVector(c1[X]*cos(2*Pi*(s+ds)), c1[X]*sin(2*Pi*(s+ds)),c1[Z]);
			dpdt=(1.0f/dt)*(p1-p0);
			p2.setVector(c0[X]*cos(2*Pi*(s+2*ds)), c0[X]*sin(2*Pi*(s+2*ds)),c0[Z]);
			dpds=(1.0f/ds)*(p2-p0);
			n=cross(dpdt,dpds); 
			n.normaliseDestructiveNoError();
			glNormal3fv(n.getArray());
			glVertex3fv(p0.getArray());
#ifdef JSON_DUMP
                        dumpString += "{\"x\":" + to_string(p0[0]) + "," + \
                                       "\"y\":" + to_string(p0[1]) + "," + \
                                       "\"z\":" + to_string(p0[2]) + "}";
                        dumpString += ",";
#endif
		}	
		glEnd();
	}
#ifdef JSON_DUMP
        dumpString.pop_back();
        dumpString += "]";
#endif

	glDisable(GL_NORMALIZE);
}

void printRounded(double t){
	const double eps=0.001;
	CVec3df v=c(t);
	if (v[X]<=0 && v[X]>=-eps) v[X]=0.0;
	if (v[Y]<=0 && v[Y]>=-eps) v[Y]=0.0;
	if (v[Z]<=0 && v[Z]>=-eps) v[Z]=0.0;
	printf("p(%.2f)=(%.2f, %.2f, %.2f)\n",t,v[X],v[Y],v[Z]);
}

void display(void)
{
	glMatrixMode( GL_MODELVIEW );	// Set the view matrix ...
	glLoadIdentity();				// ... to identity.
    gluLookAt(0,0,25, 0.5,1.5,0, 0,1,0); // camera is on the z-axis

	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	// rotate whole scene to get default view
	glRotatef(-20, 0, 1, 0);
	glRotatef(20, 1, 0, 0);
	glRotatef(-20, 0, 0, 1);
	
	glLineWidth(2.0);
	drawCoordinateSystem();


	double s=0.0f,t=0.0f;
#ifdef CODERUNNER
#include "testcode.hpp"
#endif
        /*
	printRounded(0.0);	// p(0.00)=(0.00, 0.00, 2.00)
	printRounded(0.25); // p(0.25)=(1.15, 0.00, 1.85)
	printRounded(1.0);	// p(1.00)=(3.00, 0.00, 0.00)
        */

	// draw surface of revolution
	lighting.enable();
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat_ambient_and_diffuse);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
	drawSurfaceOfRevolution();
	lighting.disable();

	// draw profile curve
	glEnable(GL_COLOR_MATERIAL);
	drawProfileCurve();
	glDisable(GL_COLOR_MATERIAL);
        compareAnswerAndModel();


	glFlush ();
#ifdef GEN_FEEDBACK
        cout << feedbackString << endl;
#else
        cout << endl;
#endif
        cout << correctnessString << endl;
#ifdef JSON_DUMP
        cout << dumpString << endl;
#endif
}

void init(void) 
{
	// select clearing color (for glClear)
	glClearColor (1,1,1,1);	// RGB-value for black
	// enable depth buffering
	glEnable(GL_DEPTH_TEST);
	// initialize view (simple orthographic projection)
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(25,1,10,30);
	// init lighting
	lighting.init();

	// material properties of the object
	mat_ambient_and_diffuse[0]=0.8;		// red material ...
	mat_ambient_and_diffuse[1]=0;
	mat_ambient_and_diffuse[2]=0;
	mat_ambient_and_diffuse[3]=1;
	mat_specular[0]=0.5f;				// ... with white highlights
	mat_specular[1]=0.5f;				// if light source is reflected
	mat_specular[2]=0.5f;				// on the material surface.
	mat_specular[3]=1;
	mat_shininess[0]=100;
}



// create a double buffered colour window
int main(int argc, char** argv)
{
#ifdef CODERUNNER
	CodeRunnerOpenGL codeRunnerOpenGL(windowWidth, windowHeight);
#else	
	glutInit(&argc, argv);		
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(windowWidth, windowHeight); 
	glutInitWindowPosition(100, 100);
	glutCreateWindow("OpenGL Transformation");
#endif
	init ();								// initialise view
#ifdef CODERUNNER
	display();
#else
	glutDisplayFunc(display);		// draw scene
	glutMainLoop();
#endif

	return 0;
}
