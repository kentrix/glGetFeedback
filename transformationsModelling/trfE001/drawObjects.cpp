// A basic OpenGL program illustrating OpenGL transformations
// Burkhard Wuensche, 6 April 2017

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <iostream>
using namespace std;
#include <math.h>
#include "Lighting.h"
#include <string.h>
#include <glm/mat4x4.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/vec4.hpp>
#include <glm/glm.hpp>

#ifdef CODERUNNER
#include "coderunneropengl.hpp"
#endif /* CODERUNNER */

const float Pi = 3.14159265358979323846264338327f;
const int windowWidth=500;
const int windowHeight=500;
const float tolerance = .001f;
static GLfloat tempMatrix[16];
static bool correct = true;

/* Drawing two houses */
#define ANSWER_SIZE 2

typedef struct AnswerMatrix {
    GLfloat* mat;
    bool checkedOff = false;
} AnswerMatrix;

AnswerMatrix answerMatrices[ANSWER_SIZE] = {};

int studentAnswerCount = 0;

// material properties of the cylinder (used if shading enabled)
GLfloat mat_specular[4];
GLfloat mat_ambient_and_diffuse[4];
GLfloat mat_shininess[1];

CLighting lighting;

void pushInAnswerMatrix() {
    static int i = 0;
    if(i >= ANSWER_SIZE) return;
    glGetFloatv(GL_MODELVIEW_MATRIX, tempMatrix); 
    answerMatrices[i].mat = (GLfloat*)malloc(sizeof(GLfloat) * 16);
    memcpy(answerMatrices[i].mat, tempMatrix, sizeof(GLfloat) * 16);
    answerMatrices[i++].checkedOff = false;
}

bool nearEqual(float a, float b) {
    return (a + tolerance > b) && (a - tolerance < b);
}

bool compareMVM(GLfloat* m, GLfloat* n, bool compareX=true, bool compareY=true, bool compareZ=true, bool compareW=true){
	if (compareX)
		for(int i=0;i<4;i++)
			if (fabs(m[i]-n[i])>tolerance) return false;
	if (compareY)
		for(int i=4;i<8;i++)
			if (fabs(m[i]-n[i])>tolerance) return false;
	if (compareZ)
		for(int i=8;i<12;i++)
			if (fabs(m[i]-n[i])>tolerance) return false;
	if (compareW)
		for(int i=12;i<16;i++)
			if (fabs(m[i]-n[i])>tolerance) return false;
	return true;
}

typedef struct GL_CALL_TABLE {
    void (*glTranslatefFunc)(GLfloat, GLfloat, GLfloat);
    void (*glRotatefFunc)(GLfloat, GLfloat, GLfloat, GLfloat);
    void (*glScalefFunc)(GLfloat, GLfloat, GLfloat);
} GL_CALL_TABLE_T;

static GL_CALL_TABLE_T callTable {
    glTranslatef, glRotatef, glScalef
};

void glRotatefDelegate(GLfloat angle, GLfloat x, GLfloat y, GLfloat z) {
    (callTable.glRotatefFunc)(angle, x, y, z);
}

void glTranslatefDelegate(GLfloat x, GLfloat y, GLfloat z) {
    (callTable.glTranslatefFunc)(x, y, z);
}

void glScalefDelegate(GLfloat x, GLfloat y, GLfloat z) {
    (callTable.glScalefFunc)(x, y, z);
}

#define glRotatef(a, b, c, d) glRotatefDelegate(a, b, c, d)
#define glTranslatef(a, b, c) glTranslatefDelegate(a, b, c)
#define glScalef(a, b, c) glScalefDelegate(a, b, c)


inline void drawString(const char* s, void* font=GLUT_BITMAP_TIMES_ROMAN_24){
	while(*s!='\0') glutBitmapCharacter(font, *(s++));
}

void drawCoordinateSystem()
{
	glColor3f(0,0,0);
	glBegin(GL_LINES);
	glVertex3f(0,0,0);
	glVertex3f(4,0,0);
	glEnd();
	glPushMatrix();
	glTranslatef(0,0,4);
	glutSolidCone(0.15, 0.4, 16, 1);
	glPopMatrix();

	glBegin(GL_LINES);
	glVertex3f(0,0,0);
	glVertex3f(0,4,0);
	glEnd();
	glPushMatrix();
	glRotatef(90,0,1,0);
	glTranslatef(0,0,4);
	glutSolidCone(0.15, 0.4, 16, 1);
	glPopMatrix();

	glBegin(GL_LINES);
	glVertex3f(0,0,0);
	glVertex3f(0,0,4);
	glEnd();
	glPushMatrix();
	glRotatef(-90,1,0,0);
	glTranslatef(0,0,4);
	glutSolidCone(0.15, 0.4, 16, 1);
	glPopMatrix();

	
	// Draw the vertex numbers next to the points
	glColor3f(0.0, 0.0, 0.0);	// black colour
	glRasterPos3f(4.3, 0.2, 0.0);
	char* s=new char[2]; drawString("x");
	glRasterPos3f(0.0, 4.3, -0.2);
	drawString("y");
	glRasterPos3f(0.0, 0.2, 4.3);
	drawString("z");
}


void drawHouse()
{
    glColor3f(1,0,0);
    glBegin(GL_LINE_STRIP);
    glVertex3f(0,2,0);
    glVertex3f(2,2,0);
    glVertex3f(1,3,0);
    glVertex3f(0,2,0);
    glVertex3f(0,0,0);
    glVertex3f(2,0,0);
    glVertex3f(2,2,0);
    glEnd();
    static int count = 0;
    // Compare with the answers now
    glGetFloatv(GL_MODELVIEW_MATRIX, tempMatrix); 

        glm::mat4x4 mat = glm::make_mat4(tempMatrix);
        glm::vec3 scale, skew, trans;
        glm::vec4 pers;
        glm::quat ori;
        glm::decompose(mat, scale, ori, trans, skew, pers);
        glm::mat4x4 mat2 = glm::make_mat4(answerMatrices[count].mat);
        glm::vec3 scale2, skew2, trans2;
        glm::vec4 pers2;
        glm::quat ori2;
        glm::decompose(mat2, scale2, ori2, trans2, skew2, pers2);
        
        if(nearEqual(scale.x, scale2.x)
                && nearEqual(scale.y, scale2.y)
                && nearEqual(scale.z, scale2.z)) {
        } else {
            correct = false;
            printf("Your scaling seems to be wrong for house number %d\t", count+1);
        }

        if(nearEqual(trans.x, trans2.x)
                && nearEqual(trans.y, trans2.y)
                && nearEqual(trans.z, trans2.z)) {
        } else {
            correct = false;
            printf("Your translation seems to be wrong for house number %d\t", count+1);
        }

        if(nearEqual(ori.x, ori2.x)
                && nearEqual(ori.y, ori2.y)
                && nearEqual(ori.z, ori2.z)
                && nearEqual(ori.w, ori2.w)) {
        } else {
            correct = false;
            printf("Your rotation seems to be wrong for house number %d\t", count+1);
        }

        count++;

        /*
        cout << "x" << scale.x << endl;
        cout << "y" << scale.y << endl;
        cout << "z" << scale.z << endl;

        cout << "x" << skew.x << endl;
        cout << "y" << skew.y << endl;
        cout << "z" << skew.z << endl;

        cout << "trans: x" << trans.x << endl;
        cout << "y" << trans.y << endl;
        cout << "z" << trans.z << endl;

        cout << "x" << pers.x << endl;
        cout << "y" << pers.y << endl;
        cout << "z" << pers.z << endl;
        cout << "w" << pers.w << endl << endl;

        cout << "x" << ori.x << endl;
        cout << "y" << ori.y << endl;
        cout << "z" << ori.z << endl;
        cout << "w" << ori.w << endl << endl;

        cout << "x" << scale2.x << endl;
        cout << "y" << scale2.y << endl;
        cout << "z" << scale2.z << endl;

        cout << "x" << skew2.x << endl;
        cout << "y" << skew2.y << endl;
        cout << "z" << skew2.z << endl;

        cout << "trans: x" << trans2.x << endl;
        cout << "y" << trans2.y << endl;
        cout << "z" << trans2.z << endl;

        cout << "x" << pers2.x << endl;
        cout << "y" << pers2.y << endl;
        cout << "z" << pers2.z << endl;
        cout << "w" << pers2.w << endl << endl;

        cout << "x" << ori2.x << endl;
        cout << "y" << ori2.y << endl;
        cout << "z" << ori2.z << endl;
        cout << "w" << ori2.w << endl << endl;
        

        if(compareMVM(tempMatrix, answerMatrices[i].mat)) {
            answerMatrices[i].checkedOff = true;
            break;
        }

        */
}

void display(void)
{
	glMatrixMode( GL_MODELVIEW );	// Set the view matrix ...
	glLoadIdentity();				// ... to identity.
        gluLookAt(0,0,20, 0,1.25,0, 0,1,0); // camera is on the z-axis

	// rotate whole scene

	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glRotatef(-20, 0, 1, 0);
	glRotatef(30, 1, 0, 0);
	glRotatef(-30, 0, 0, 1);
	
	glLineWidth(2.0);
	drawCoordinateSystem();

        /* Setup answers */
        glPushMatrix();
	glScalef(1, 2, 1);
	glRotatef(90, 0, 0, 1);	
        pushInAnswerMatrix();
	glRotatef(-90, 0, 1, 0);
        pushInAnswerMatrix();
        glPopMatrix();

        //glLoadIdentity();

#ifdef CODERUNNER
//#include "testcode.hpp"
#include "answer.hpp"
#else
	glScalef(1, 2, 1);
        glTranslatef(1,1,1);
	glRotatef(90, 0, 0, 1);	
	drawHouse();
	glRotatef(-90, 0, 1, 0);
	drawHouse();
#endif

	glDisable(GL_NORMALIZE);
	glFlush ();

        printf("\n");

        if(correct) printf("1\n");
        else printf("0\n");

        /*
        for(int i = 0; i < ANSWER_SIZE; i++) {
            if(!answerMatrices[i].checkedOff) {
                printf("0\n");
                return;
            }
        }
        printf("1\n");
        */
}

void init(void) 
{
	// select clearing color (for glClear)
	glClearColor (1,1,1,1);	// RGB-value for black
	// enable depth buffering
	glEnable(GL_DEPTH_TEST);
	// initialize view (simple orthographic projection)
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(25,1,10,30);

	// material properties of the object
	mat_ambient_and_diffuse[0]=0.8;		// red material ...
	mat_ambient_and_diffuse[1]=0;
	mat_ambient_and_diffuse[2]=0;
	mat_ambient_and_diffuse[3]=1;
	mat_specular[0]=0.5f;				// ... with white highlights
	mat_specular[1]=0.5f;				// if light source is reflected
	mat_specular[2]=0.5f;				// on the material surface.
	mat_specular[3]=1;
	mat_shininess[0]=100;
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat_ambient_and_diffuse);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);

//	lighting.enable();
//	glShadeModel(GL_SMOOTH);
}



// create a double buffered colour window
int main(int argc, char** argv)
{
#ifdef CODERUNNER
	CodeRunnerOpenGL codeRunnerOpenGL(windowWidth, windowHeight);
#else	
	glutInit(&argc, argv);		
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(windowWidth, windowHeight); 
	glutInitWindowPosition(100, 100);
	glutCreateWindow("OpenGL Transformation");
#endif
	init ();								// initialise view
#ifdef CODERUNNER
	display();
#else
	glutDisplayFunc(display);		// draw scene
	glutMainLoop();
#endif

	return 0;
}
