// A basic OpenGL program illustrating OpenGL transformations
// Burkhard Wuensche, 6 April 2017

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <iostream>
using namespace std;
#include <math.h>
#include "Lighting.h"

#ifdef CODERUNNER
#include "coderunneropengl.hpp"
#endif /* CODERUNNER */

const float Pi = 3.14159265358979323846264338327f;
const int windowWidth=500;
const int windowHeight=500;

// material properties of the cylinder (used if shading enabled)
GLfloat mat_specular[4];
GLfloat mat_ambient_and_diffuse[4];
GLfloat mat_shininess[1];

CLighting lighting;

GLfloat mvmInput[16];			// modelview matrix using code input into Coderunner
GLfloat mvmSolution[16];		// modelview matrix for sample solution
const double tolerance=0.01;	// tolerance for comparing modelview matrices

typedef struct GL_CALL_TABLE {
    void (*glTranslatefFunc)(GLfloat, GLfloat, GLfloat);
    void (*glRotatefFunc)(GLfloat, GLfloat, GLfloat, GLfloat);
    void (*glScalefFunc)(GLfloat, GLfloat, GLfloat);
} GL_CALL_TABLE_T;

static GL_CALL_TABLE_T callTable {
    glTranslatef, glRotatef, glScalef
};

void glRotatefDelegate(GLfloat angle, GLfloat x, GLfloat y, GLfloat z) {
    printf("Deletegate %f, %f, %f \n", x, y, z);
    (callTable.glRotatefFunc)(angle, x, y, z);
}

void glTranslatefDelegate(GLfloat x, GLfloat y, GLfloat z) {
    (callTable.glTranslatefFunc)(x, y, z);
}

void glScalefDelegate(GLfloat x, GLfloat y, GLfloat z) {
    (callTable.glScalefFunc)(x, y, z);
}

#define glRotatef(a, b, c, d) glRotatefDelegate(a, b, c, d)
#define glTranslatef(a, b, c) glTranslatefDelegate(a, b, c)
#define glScalef(a, b, c) glScalefDelegate(a, b, c)

//XXX: We can either do this or use APITrace
//XXX: Using APItrace might mean that we will be parsing texual data

bool compareMVM(GLfloat* m, GLfloat* n, bool compareX=true, bool compareY=true, bool compareZ=true, bool compareW=true){
	if (compareX)
		for(int i=0;i<4;i++)
			if (fabs(m[i]-n[i])>tolerance) return false;
	if (compareY)
		for(int i=4;i<8;i++)
			if (fabs(m[i]-n[i])>tolerance) return false;
	if (compareZ)
		for(int i=8;i<12;i++)
			if (fabs(m[i]-n[i])>tolerance) return false;
	if (compareW)
		for(int i=12;i<16;i++)
			if (fabs(m[i]-n[i])>tolerance) return false;
	return true;
}

inline void drawString(const char* s, void* font=GLUT_BITMAP_TIMES_ROMAN_24){
	while(*s!='\0') glutBitmapCharacter(font, *(s++));
}

void drawCoordinateSystem()
{
	glLineWidth(3.0);

	glColor3f(0,0,0);
	glBegin(GL_LINES);
	glVertex3f(0,0,0);
	glVertex3f(4.5,0,0);
	glEnd();
	glPushMatrix();
	glTranslatef(0,0,4.5);
	glutSolidCone(0.15, 0.4, 16, 1);
	glPopMatrix();

	glBegin(GL_LINES);
	glVertex3f(0,0,0);
	glVertex3f(0,4.5,0);
	glEnd();
	glPushMatrix();
	glRotatef(90,0,1,0);
	glTranslatef(0,0,4.5);
	glutSolidCone(0.15, 0.4, 16, 1);
	glPopMatrix();

	glBegin(GL_LINES);
	glVertex3f(0,0,0);
	glVertex3f(0,0,4.5);
	glEnd();
	glPushMatrix();
	glRotatef(-90,1,0,0);
	glTranslatef(0,0,4.5);
	glutSolidCone(0.15, 0.4, 16, 1);
	glPopMatrix();

	glLineWidth(1.0);
	int i;
	// grid for 1st quadrant of xy-plane (positive x, positive y)
	glBegin(GL_LINES);
	for(i=1;i<=4;i++){
		glVertex3f(0,i,0);
		glVertex3f(4,i,0);
		glVertex3f(i,0,0);
		glVertex3f(i,4,0);
	}
	glEnd();

	// grid for 1st quadrant of xz-plane (positive x, positive z)
	glBegin(GL_LINES);
	for(i=1;i<=4;i++){
		glVertex3f(0,0,i);
		glVertex3f(4,0,i);
		glVertex3f(i,0,0);
		glVertex3f(i,0,4);
	}
	glEnd();

	// grid for 1st quadrant of yz-plane (positive y, positive z)
	glBegin(GL_LINES);
	for(i=1;i<=4;i++){
		glVertex3f(0,0,i);
		glVertex3f(0,4,i);
		glVertex3f(0,i,0);
		glVertex3f(0,i,4);
	}
	glEnd();

	// Draw the vertex numbers next to the points
	glColor3f(0.0, 0.0, 0.0);	// black colour
	glRasterPos3f(4.8, 0.2, 0.0);
	char* s=new char[2]; drawString("x");
	glRasterPos3f(0.2, 4.8, -0.2);
	drawString("y");
	glRasterPos3f(-0.3, 0.2, 4.8);
	drawString("z");
}

// for debugging purposes - not used
void outputMVM(GLfloat* m)
{
	cout << "\n" << m[0] << " " << m[1] << " " << m[2] << " " << m[3];
	cout << "\n" << m[4] << " " << m[5] << " " << m[6] << " " << m[7];
	cout << "\n" << m[8] << " " << m[9] << " " << m[10] << " " << m[11];
	cout << "\n" << m[12] << " " << m[13] << " " << m[14] << " " << m[15];
}

void drawShape()
{
	glLineWidth(3.0);
	glColor3f(1,0,0);
	glBegin(GL_LINE_LOOP);
	glVertex3f(2,2,0);
	glVertex3f(1,2,0);
	glVertex3f(0,1,0);
	glVertex3f(0.5,1,0);
	glVertex3f(1,1.5,0);
	glVertex3f(1,0,0);
	glVertex3f(2,0,0);
	glEnd();
}

void display(void)
{
	glMatrixMode( GL_MODELVIEW );	// Set the view matrix ...
	glLoadIdentity();				// ... to identity.
    gluLookAt(0,0,25, 0.5,1.5,0, 0,1,0); // camera is on the z-axis

	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// rotate whole scene to get default view
	glRotatef(-20, 0, 1, 0);
	glRotatef(20, 1, 0, 0);
	glRotatef(-20, 0, 0, 1);
	
	// draw coordinate system and grid
	drawCoordinateSystem();

	// SAMPLE SOLUTION - store resulting modelview matrix in mvmSolution
	glPushMatrix();
	glRotatef(135,0,0,1);	// Sample solution line 1
	glTranslatef(-1,-2,0);	// Sample solution line 2
	glGetFloatv(GL_MODELVIEW_MATRIX, mvmSolution); 
	glPopMatrix();

#ifdef CODERUNNER
//#include "testcode.hpp"
#include "answer.hpp"
#else
//	sample solution 1
/*	glRotatef(135,0,0,1);
	glTranslatef(-1,-2,0);*/
//	sample solution 2
	glScalef(1,1,-1);
	glRotatef(135,0,0,1);
	glTranslatef(-1,-2,0);

#endif
	// output whether student solution is correct - this is used for the Coderrunner test case
	glGetFloatv(GL_MODELVIEW_MATRIX, mvmInput); 
	cout << compareMVM(mvmInput,mvmSolution,true,true,false,true);
	// draw resulting trasformed shape
	drawShape();

	glDisable(GL_NORMALIZE);
	glFlush ();
}

void init(void) 
{
	// select clearing color (for glClear)
	glClearColor (1,1,1,1);	// RGB-value for black
	// enable depth buffering
	glEnable(GL_DEPTH_TEST);
	// initialize view (simple orthographic projection)
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(25,1,10,30);

	// material properties of the object
	mat_ambient_and_diffuse[0]=0.8;		// red material ...
	mat_ambient_and_diffuse[1]=0;
	mat_ambient_and_diffuse[2]=0;
	mat_ambient_and_diffuse[3]=1;
	mat_specular[0]=0.5f;				// ... with white highlights
	mat_specular[1]=0.5f;				// if light source is reflected
	mat_specular[2]=0.5f;				// on the material surface.
	mat_specular[3]=1;
	mat_shininess[0]=100;
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat_ambient_and_diffuse);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);

//	lighting.enable();
//	glShadeModel(GL_SMOOTH);
}


// create a double buffered colour window
int main(int argc, char** argv)
{

#ifdef CODERUNNER
    CodeRunnerOpenGL codeRunnerOpenGL(windowWidth, windowHeight);
#else	
    glutInit(&argc, argv);		
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(windowWidth, windowHeight); 
    glutInitWindowPosition(100, 100);
    glutCreateWindow("OpenGL Transformation");
#endif
    init();								// initialise view

#ifdef CODERUNNER
    display();
#else
    glutDisplayFunc(display);		// draw scene
    glutMainLoop();
#endif

    return 0;
}
